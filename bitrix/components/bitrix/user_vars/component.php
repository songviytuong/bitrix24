<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if (!CModule::IncludeModule("user_vars")) {
    ShowError(GetMessage("MODULE_NOT_INSTALL"));
    return;
}
IncludeModuleLangFile(__FILE__);
CModule::IncludeModule('user_vars');

if ($_REQUEST["DELETE"] == "Y") {
    UserVars::Delete($_REQUEST["params"]);
}

if (!CModule::IncludeModule("user_vars")) {
    $this->AbortResultCache();
    ShowError(GetMessage("USER_VARS_MODULE_NOT_INSTALLED"));
    return;
}

$arParams["PATH_TO_VARS_LIST"] = Trim($arParams["PATH_TO_VARS_LIST"]);
if (StrLen($arParams["PATH_TO_VARS_LIST"]) <= 0)
    $arParams["PATH_TO_VARS_LIST"] = HtmlSpecialCharsbx($APPLICATION->GetCurPage());

$arParams["SET_TITLE"] = ($arParams["SET_TITLE"] == "Y" ? "Y" : "N");
if ($arParams["SET_TITLE"] == "Y")
    $APPLICATION->SetTitle(GetMessage("PS_TITLE"));

$arResult = array();
$arResult["LIST"] = UserVars::GetList();

$arResult["ALLOWED_FIELDS"] = array(
    "NAME" => array(
        "NAME" => GetMessage("COL_NAME"),
        "ORDERABLE" => true,
        "FILTERABLE" => true,
        "TYPE" => "string",
        "IS_FIELD" => true,
    ),
    "VALUE" => array(
        "NAME" => GetMessage("COL_VALUE"),
        "ORDERABLE" => false,
        "FILTERABLE" => false,
        "TYPE" => "string",
        "IS_FIELD" => true,
    ),
    "DESCRIPTION" => array(
        "NAME" => GetMessage("COL_DESCRIPTION"),
        "ORDERABLE" => false,
        "FILTERABLE" => false,
        "TYPE" => "string",
        "IS_FIELD" => true,
    ),
);

$arAction = array();

CComponentEngine::MakePathFromTemplate($arParams["VARS_URL"], array());
$arParams['VARS_URL'] = $arParams['VARS_URL'] ? $arParams['VARS_URL'] : '?params=#PARAMS#';

//echo "<pre>".print_r($arResult, true)."</pre>";

$this->IncludeComponentTemplate();
?>