<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
if (!$USER->CanDoOperation('edit_own_profile') && !$USER->CanDoOperation('edit_other_settings') && !$USER->CanDoOperation('view_other_settings'))
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$isAdmin = $USER->CanDoOperation('edit_other_settings');
CModule::IncludeModule('lipit');
IncludeModuleLangFile(__FILE__);

$sTableID = "tbl_lipit";
if ($isAdmin)
    $oSort = new CAdminSorting($sTableID, "id", "desc");
else
    $oSort = new CAdminSorting($sTableID, "sort", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);

function CheckFilter() {
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $f)
        global $$f;
    $date_1_ok = false;
    $date1_stm = MkDateTime(FmtDate($find_date1, "D.M.Y"), "d.m.Y");
    $date2_stm = MkDateTime(FmtDate($find_date2, "D.M.Y") . " 23:59", "d.m.Y H:i");
    if (!$date1_stm && strlen(trim($find_date1)) > 0)
        $lAdmin->AddFilterError(GetMessage("MAIN_WRONG_DATE_FROM"));
    else
        $date_1_ok = true;
    if (!$date2_stm && strlen(trim($find_date2)) > 0)
        $lAdmin->AddFilterError(GetMessage("MAIN_WRONG_DATE_TILL"));
    elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
        $lAdmin->AddFilterError(GetMessage("MAIN_FROM_TILL_DATE"));
    return count($lAdmin->arFilterErrors) == 0;
}

$FilterArr = Array(
    "find",
    "find_name",
    "find_id",
);

$lAdmin->InitFilter($FilterArr);

$arFilter = Array();
if (CheckFilter()) {
    $arFilter = Array(
        "L_Name"    => ($find!="" && $find_type == "find_name"? $find:$find_name),
        "L_ID"      => ($find!="" && $find_type == "find_id"? $find:$find_id),
    );
}

//var_dump($arFilter);

if ($lAdmin->EditAction()) {
    foreach ($FIELDS as $ID => $arFields) {
        if($ID <= 0)
                continue;
        if(!$lAdmin->IsUpdated($ID))
                continue;
        if (!LipitController::Update('a_lipit', array('L_ID' => $ID), $arFields)) {
            $e = $APPLICATION->GetException();
            $lAdmin->AddUpdateError(($e ? $e->GetString() : GetMessage("UPDATE_ERROR")), $ID);
        }
    }
}

if (($arID = $lAdmin->GroupAction())) {
    if ($_REQUEST['action_target'] == 'selected') {
        $rsData = LipitController::GetList('*','a_lipit','1',$arFilter);
        foreach($rsData as $arRes){
            $arID[] = $arRes['L_ID'];
        }
    } else {
        //REQUEST FROM EDIT PAGE
        $arID[] = $params;
    }
    
    foreach ($arID as $ID) {
        if ($ID <= 0)
            continue;
        switch ($_REQUEST['action']) {
            case "delete":
                if (!LipitController::Delete('a_lipit',array('L_ID' => $ID)))
                    $lAdmin->AddGroupError(GetMessage("REMOVE_ERROR"), $params);
                break;
        }
    }
}

$rsData = LipitController::GetList('*','a_lipit','1',$arFilter);
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("list_nav")));

if ($isAdmin) {
    $aHeaders[] = array("id" => "L_ID", "content" => GetMessage("COL_ID"), "sort" => "L_ID", "default" => true);
}

$aHeaders = array(
    array("id" => "L_Name", "content" => GetMessage("COL_NAME"), "sort" => "L_Name", "default" => true)
);

$lAdmin->AddHeaders($aHeaders);

while ($arRes = $rsData->NavNext(true, "f_")) {
    $row = & $lAdmin->AddRow($f_L_ID, $arRes);
    $row->AddViewField("L_ID", $f_L_ID);
    $row->AddInputField("L_Name", array("size" => 20));
    $row->AddViewField("L_Name", $f_L_Name);

    $arActions = Array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => GetMessage("EDIT"),
            "ACTION" => $lAdmin->ActionRedirect("lipit_list_edit.php?params=" . $f_L_ID)
        ),
        array("SEPARATOR" => true),
        array(
            "ICON" => "delete",
            "TEXT" => GetMessage("DELETE"),
            "ACTION" => "if(confirm('" . GetMessage("delete_conf") . "')) " . $lAdmin->ActionDoGroup($f_L_ID, "delete")
        ),
    );

    $row->AddActions($arActions);
}

$lAdmin->AddGroupActionTable(Array(
    "delete" => true,
    //"activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"),
    //"deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"),
));

$aContext = array(
    array(
        "TEXT" => GetMessage("ADD"),
        "LINK" => "lipit_list_edit.php?lang=" . LANG,
        "TITLE" => GetMessage("ADD"),
        "ICON" => "btn_new",
    ),
);

$lAdmin->AddAdminContextMenu($aContext);
$lAdmin->CheckListMode();


$APPLICATION->SetTitle(GetMessage("LIST_MENU"));
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?php
$arFRows = array(
    "find_id" => GetMessage("find_id"),
    "find_name" => GetMessage("find_name")
);
$oFilter = new CAdminFilter($sTableID . "_filter", $arFRows);
?>
<form name="form1" method="GET" action="<?= $APPLICATION->GetCurPage() ?>">
    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
<?php $oFilter->Begin(); ?>
    <tr>
        <td><b><?php echo GetMessage("find") ?></b></td>
        <td>
            <input type="text" size="25" name="find" value="<?echo htmlspecialcharsbx($find)?>" title="<?echo GetMessage("fav_list_flt_find_title")?>">
<?php
$arr = array(
    "reference" => array(
        GetMessage("find_name"),
        GetMessage("find_id")
    ),
    "reference_id" => array(
        
        "find_name",
        "find_id"
    )
);
echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
?>
        </td>
    </tr>
    <tr>
        <td><?echo GetMessage("COL_ID")?></td>
        <td><input type="text" name="find_id" size="40" value="<?echo htmlspecialcharsbx($find_id)?>"><?= ShowFilterLogicHelp() ?></td>
    </tr>
    <tr>
        <td><?echo GetMessage("COL_NAME")?></td>
        <td><input type="text" name="find_name" size="40" value="<?echo htmlspecialcharsbx($find_name)?>"><?= ShowFilterLogicHelp() ?></td>
    </tr>
<?php
$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "form1"));
$oFilter->End();
?>
</form>
<?php $lAdmin->DisplayList(); ?>

<?php require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>