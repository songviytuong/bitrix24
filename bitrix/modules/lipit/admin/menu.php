<?php
IncludeModuleLangFile(__FILE__);
global $APPLICATION;
if($APPLICATION->GetGroupRight("lipit")>"D")
{
$aMenu[] = array(
    "parent_menu" => "global_menu_content",
    "section" => "lipit",
    "sort" => 1,
    "text" => GetMessage("LIPIT_MODULE"),
    "title" => GetMessage("LIPIT_MODULE"),
    "icon" => "user_menu_icon",
    "page_icon" => "user_menu_icon",
    "module_id" => "lipit",
    "items_id" => "menu_lipit",
    "items" => array(
            array(
                "text" => GetMessage('LIST_TITLE'),
                "url" => "lipit_list.php?lang=" . LANGUAGE_ID,
                "more_url" => Array("lipit_list.php"),
                "title" => GetMessage('LIST_TITLE'),
            ),
            array(
                "text" => GetMessage("LIST_UNIT"),
                "url" => "lipit_list_unit.php?lang=" . LANGUAGE_ID,
                "more_url" => Array("lipit_list_unit.php","lipit_list_unit_edit.php"),
                "title" => GetMessage("LIST_UNIT"),
            ),
        )
    );
return $aMenu;
}
return false;