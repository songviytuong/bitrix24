<?php
$MESS ['LIST_TITLE'] = "List";
$MESS ['list_nav'] = "Lipit nav";
$MESS ['COL_ID'] = "ID";
$MESS ['COL_NAME'] = "NAME";
$MESS ['delete_conf'] = "Delete?";
$MESS ['ADD'] = "Add";
$MESS ['EDIT'] = "Edit";
$MESS ['DELETE'] = "Delete";
$MESS ['REMOVE_ERROR'] = "Remove Error...";
$MESS ['UPDATE_ERROR'] = "Update Error...";
$MESS ['find_id'] = "ID";
$MESS ['find_name'] = "Name";