<?php

CModule::IncludeModule('form');

class LipitController extends CAllOption {

    const MODULE_NAME = "lipit";
    const TABLE_LIPIT = "a_lipit";
    const TABLE_LIPIT_DETAIL = "a_lipit_detail";
    const TABLE_LIPIT_UNIT = "a_lipit_unit";

    public static function GetList($select = "*", $from_table = self::TABLE_LIPIT, $where = 1, $arFilter = array()) {
        global $DB;
        $strSqlSearch = "";
        $url = CAdvBanner::GetCurUri();
        if (is_array($arFilter)) {
            $filter_keys = array_keys($arFilter);
            for ($i = 0, $n = count($filter_keys); $i < $n; $i++) {
                $key = $filter_keys[$i];
                $val = $arFilter[$filter_keys[$i]];
                if($val == ''){
                    $where = 1;
                }
                if (is_array($val) && count($val) <= 0)
                    continue;
                if ($val == '' || $val == "NOT_REF")
                    continue;
                $match_value_set = (in_array($key . "_EXACT_MATCH", $filter_keys)) ? true : false;
                $key = strtoupper($key);
                switch ($key) {
                    case "L_ID":
                        $match = ($arFilter[$key . "_EXACT_MATCH"] == "N" && $match_value_set) ? "Y" : "N";
                        $arSqlSearch[] = GetFilterQuery("L_ID", $val, $match);
                        break;

                    case "L_NAME":
                        $match = ($arFilter[$key . "_EXACT_MATCH"] == "Y" && $match_value_set) ? "N" : "Y";
                        $arSqlSearch[] = GetFilterQuery($key, $val, $match);
                        break;
                }
                $strSqlSearch = GetFilterSqlSearch($arSqlSearch);
            }
        }
        
        if ($arFilter && $strSqlSearch) {
            $where = $strSqlSearch;
        }

        $result = array();
        $error_mess = (CForm::err_mess()) . "<br>Function: GetList($from_table)<br>Line: ";
        $query = "SELECT " . $select . " FROM " . $from_table . " WHERE $where";
        $res = $DB->Query($query, false, $err_mess . __LINE__);
        while ($arr = $res->Fetch()) {
            $result[] = $arr;
        }
        return $result;
    }

    public static function GetListByParams($from_table = "", $params = "") {
        
    }
    
    public static function GetDetail($params) {
        
    }

    public static function Delete($from_table = self::TABLE_LIPIT, $params = array('L_ID' => 1)) {
        global $DB;

        $where = "";
        $numItems = count($params);
        $i = 0;
        foreach ($params as $key => $item) {
            if (++$i === $numItems) {
                $and = "";
            } else {
                $and = " and ";
            }
            $where .= "`" . $key . "` = '" . $item . "'" . $and;
        }

        return ($DB->Query("DELETE FROM `$from_table` WHERE $where", false, "File: " . __FILE__ . "<br>Function: Delete()<br>Line: " . __LINE__));
    }

    public static function Update($from_table = "", $params = array(), $arFields) {
        global $DB;
        $strUpdate = $DB->PrepareUpdate($from_table, $arFields);

        $where = "";
        $numItems = count($params);
        $i = 0;
        foreach ($params as $key => $item) {
            if (++$i === $numItems) {
                $and = "";
            } else {
                $and = " and ";
            }
            $where .= "`" . $key . "` = '" . $item . "'" . $and;
        }
        if ($strUpdate != "") {
            $strSql = "UPDATE `$from_table` SET " . $strUpdate . " WHERE $where";
            if (!$DB->Query($strSql))
                return false;
        }
        return true;
    }

    public static function GetModuleName() {
        return self::MODULE_NAME;
    }

}
