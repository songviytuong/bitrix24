<?php
global $DB, $MESS, $APPLICATION;
IncludeModuleLangFile(__FILE__);
$DBType = strtolower($DB->type);
$arClassesList = array(
  "LipitController" => "classes/general/LipitController.php",
);

CModule::AddAutoloadClasses(
  "lipit",
  $arClassesList
);