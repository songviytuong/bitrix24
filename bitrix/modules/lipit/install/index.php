<?php

/*  Create Module Custom (lipit)
 *  Date: 01-11-2016
 *  Author: Lipit
 */
global $MESS;
$PathInstall = str_replace("\\", "/", __FILE__);
$PathInstall = substr($PathInstall, 0, strlen($PathInstall) - strlen("/index.php"));
IncludeModuleLangFile($PathInstall . "/install.php");

if (class_exists("lipit"))
    return;

Class lipit extends CModule {

    var $MODULE_ID = "lipit";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";

    function lipit() {

        $this->MODULE_VERSION = '1.0.1';
        $this->MODULE_VERSION_DATE = '01-11-2016';
        $this->MODULE_NAME = GetMessage("MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("MODULE_DESCRIPTION");

        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = '-';
            $this->MODULE_VERSION_DATE = '-';
        }

        $this->MODULE_NAME = GetMessage("MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("MODULE_DESCRIPTION");
    }

    function DoInstall() {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->InstallFiles();
        RegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile(GetMessage('INSTALL_TITLE'), $DOCUMENT_ROOT . "/bitrix/modules/$this->MODULE_ID/install/step.php");
    }

    function InstallFiles($arParams = array()) {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->MODULE_ID."/install/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true);
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/".$this->MODULE_ID."/install/admin/", $_SERVER['DOCUMENT_ROOT'] . "/bitrix/admin");
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->MODULE_ID."/install/images", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/images/$this->MODULE_ID/", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->MODULE_ID."/install/themes/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes", true, true);
        return true;
    }

    function UnInstallFiles() {
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/'.$this->MODULE_ID.'/install/admin/', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/'.$this->MODULE_ID.'/install/themes/.default/', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/themes/.default'); //css
        DeleteDirFilesEx('/bitrix/themes/.default/icons/'.$this->MODULE_ID.'/'); //icons
        DeleteDirFilesEx('/bitrix/images/'.$this->MODULE_ID.'/'); //images
        return true;
    }

    function DoUninstall() {
        global $DOCUMENT_ROOT, $APPLICATION;
        //$this->UnInstallDB();
        $this->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/$this->MODULE_ID/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/$this->MODULE_ID/install/themes/.default/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes/.default"); //css
        DeleteDirFilesEx("/bitrix/themes/.default/icons/$this->MODULE_ID/"); //icons
        DeleteDirFilesEx("/bitrix/images/$this->MODULE_ID/"); //images
        DeleteDirFilesEx("/bitrix/components/bitrix/$this->MODULE_ID/"); //components

        $APPLICATION->IncludeAdminFile(GetMessage('UNINSTALL_TITLE'), $DOCUMENT_ROOT . "/bitrix/modules/$this->MODULE_ID/install/unstep.php");
    }

    function UnInstallDB() {
        global $DB, $DBType, $APPLICATION;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/$this->MODULE_ID/install/db/" . $DBType . "/uninstall.sql");
        if ($this->errors !== false) {
            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }
        return true;
    }
}
?>