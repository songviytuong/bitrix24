<?php
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2013 Bitrix
 */

require_once(substr(__FILE__, 0, strlen(__FILE__) - strlen("/include.php"))."/bx_root.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/start.php");

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/virtual_io.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/virtual_file.php");


$application = \Bitrix\Main\Application::getInstance();
$application->initializeExtendedKernel(array(
	"get" => $_GET,
	"post" => $_POST,
	"files" => $_FILES,
	"cookie" => $_COOKIE,
	"server" => $_SERVER,
	"env" => $_ENV
));

//define global application object
$GLOBALS["APPLICATION"] = new CMain;

if(defined("SITE_ID"))
	define("LANG", SITE_ID);

if(defined("LANG"))
{
	if(defined("ADMIN_SECTION") && ADMIN_SECTION===true)
		$db_lang = CLangAdmin::GetByID(LANG);
	else
		$db_lang = CLang::GetByID(LANG);

	$arLang = $db_lang->Fetch();

	if(!$arLang)
	{
		throw new \Bitrix\Main\SystemException("Incorrect site: ".LANG.".");
	}
}
else
{
	$arLang = $GLOBALS["APPLICATION"]->GetLang();
	define("LANG", $arLang["LID"]);
}

$lang = $arLang["LID"];
if (!defined("SITE_ID"))
	define("SITE_ID", $arLang["LID"]);
define("SITE_DIR", $arLang["DIR"]);
define("SITE_SERVER_NAME", $arLang["SERVER_NAME"]);
define("SITE_CHARSET", $arLang["CHARSET"]);
define("FORMAT_DATE", $arLang["FORMAT_DATE"]);
define("FORMAT_DATETIME", $arLang["FORMAT_DATETIME"]);
define("LANG_DIR", $arLang["DIR"]);
define("LANG_CHARSET", $arLang["CHARSET"]);
define("LANG_ADMIN_LID", $arLang["LANGUAGE_ID"]);
define("LANGUAGE_ID", $arLang["LANGUAGE_ID"]);

$context = $application->getContext();
$context->setLanguage(LANGUAGE_ID);
$context->setCulture(new \Bitrix\Main\Context\Culture($arLang));

$request = $context->getRequest();
if (!$request->isAdminSection())
{
	$context->setSite(SITE_ID);
}

$application->start();

$GLOBALS["APPLICATION"]->reinitPath();

if (!defined("POST_FORM_ACTION_URI"))
{
	define("POST_FORM_ACTION_URI", htmlspecialcharsbx(GetRequestUri()));
}

$GLOBALS["MESS"] = array();
$GLOBALS["ALL_LANG_FILES"] = array();
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/tools.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/database.php");
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/main.php");
IncludeModuleLangFile(__FILE__);

error_reporting(COption::GetOptionInt("main", "error_reporting", E_COMPILE_ERROR|E_ERROR|E_CORE_ERROR|E_PARSE) & ~E_STRICT & ~E_DEPRECATED);

if(!defined("BX_COMP_MANAGED_CACHE") && COption::GetOptionString("main", "component_managed_cache_on", "Y") <> "N")
{
	define("BX_COMP_MANAGED_CACHE", true);
}

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/filter_tools.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/ajax_tools.php");

/*ZDUyZmZMjU2MGU5ZjQwY2QyNGQ1MmZiMTNhY2FlYTE0MDY4ZGI=*/$GLOBALS['_____964133203']= array(base64_decode('R2V0T'.'W9kdW'.'xlRXZlbnR'.'z'),base64_decode('RX'.'hlY3V0ZU1v'.'ZHV'.'sZUV2ZW50RXg'.'='));; $GLOBALS['____75015782']= array(base64_decode('ZGVmaW5l'),base64_decode(''.'c'.'3RybG'.'Vu'),base64_decode(''.'Ym'.'FzZTY0X2R'.'lY29k'.'ZQ=='),base64_decode('dW5'.'zZXJpYW'.'xpem'.'U='),base64_decode('aXNf'.'YXJyYXk='),base64_decode('Y2'.'91bnQ='),base64_decode('aW'.'5fYXJyY'.'X'.'k='),base64_decode('c2'.'V'.'yaW'.'FsaXpl'),base64_decode('YmFz'.'ZTY0'.'X'.'2VuY29k'.'ZQ=='),base64_decode('c3RybGVu'),base64_decode('YXJ'.'yYXlfa2V'.'5X2'.'V4aX'.'N'.'0c'.'w=='),base64_decode('Y'.'XJyYXlfa2V5X2'.'V4aXN0cw=='),base64_decode('bWt0a'.'W1l'),base64_decode(''.'ZG'.'F'.'0ZQ=='),base64_decode('ZGF0ZQ=='),base64_decode('YXJyYX'.'lfa2V'.'5X2V4aXN'.'0'.'cw=='),base64_decode('c3'.'RybGVu'),base64_decode('YXJ'.'yYXlfa2V5X2V4a'.'XN0'.'cw=='),base64_decode('c3R'.'yb'.'GVu'),base64_decode('YXJyYXl'.'fa2V5X2V4aXN0cw=='),base64_decode('YXJyYXlfa'.'2V'.'5X2V4a'.'XN'.'0cw=='),base64_decode('bWt'.'0aW1'.'l'),base64_decode('ZGF0ZQ=='),base64_decode('ZG'.'F0ZQ'.'=='),base64_decode('bWV'.'0aG'.'9kX2V4aXN0'.'c'.'w'.'=='),base64_decode('Y2FsbF'.'91c2VyX'.'2Z1bmN'.'fYXJyYXk='),base64_decode('c3RybGVu'),base64_decode(''.'YXJyYXlfa'.'2V5X2V4aXN0cw=='),base64_decode(''.'YX'.'JyYXlfa2V'.'5X2V4aXN'.'0cw'.'=='),base64_decode(''.'c2'.'VyaW'.'FsaX'.'p'.'l'),base64_decode('YmFzZTY'.'0X2VuY29k'.'ZQ'.'='.'='),base64_decode('c3'.'Ryb'.'GVu'),base64_decode('YXJyYXlfa2V5X2V4'.'aX'.'N'.'0cw=='),base64_decode('YX'.'JyY'.'Xlfa2'.'V5X2'.'V4aXN0cw=='),base64_decode('YXJy'.'YXlfa2V5X2'.'V4aXN0c'.'w=='),base64_decode(''.'aX'.'NfYXJyYXk='),base64_decode('YX'.'Jy'.'YXlf'.'a2'.'V'.'5'.'X2V4aXN0cw=='),base64_decode('c2VyaWFs'.'aX'.'pl'),base64_decode('YmFz'.'ZT'.'Y0X2V'.'uY'.'29kZ'.'Q='.'='),base64_decode('Y'.'XJyYXl'.'fa2V5X2V4'.'aXN0cw'.'='.'='),base64_decode('YX'.'JyYXlfa2V5X2'.'V4'.'aXN0c'.'w=='),base64_decode('c2VyaWFsaXpl'),base64_decode('YmF'.'zZTY0X2VuY29kZQ=='),base64_decode('aXNf'.'Y'.'XJy'.'YXk'.'='),base64_decode('aXNfYXJyYXk='),base64_decode('aW5'.'fY'.'XJy'.'YXk='),base64_decode('YXJyYXl'.'f'.'a2V5'.'X2'.'V4aXN0c'.'w=='),base64_decode(''.'aW5f'.'YXJyYXk='),base64_decode(''.'b'.'W'.'t0aW1l'),base64_decode('ZGF0'.'ZQ'.'=='),base64_decode(''.'Z'.'G'.'F0ZQ=='),base64_decode('ZG'.'F0ZQ=='),base64_decode('bWt0aW'.'1l'),base64_decode('ZG'.'F0ZQ='.'='),base64_decode('ZGF0ZQ=='),base64_decode('aW'.'5'.'fYXJyYXk='),base64_decode('YXJyYX'.'lfa2V5X2V4aX'.'N'.'0cw=='),base64_decode('YXJyYX'.'l'.'fa2'.'V5'.'X'.'2V4aXN0cw=='),base64_decode('c2VyaWFsaXpl'),base64_decode('YmFzZTY0X2VuY'.'29kZQ=='),base64_decode('YXJyYXl'.'f'.'a2V5X2V4aX'.'N0'.'cw=='),base64_decode('aW'.'50d'.'mFs'),base64_decode(''.'dGltZQ'.'=='),base64_decode('YXJyY'.'Xlfa2V5X2V4'.'aXN0'.'cw'.'=='),base64_decode('Zm'.'l'.'sZV9leGlzdHM'.'='),base64_decode(''.'c'.'3R'.'yX3Jlc'.'Gx'.'hY2'.'U'.'='),base64_decode('Y2x'.'hc3N'.'fZXhp'.'c3R'.'z'),base64_decode('ZGVmaW5l'));; function ___1327645095($_1384917633){static $_1324923403= false; if($_1324923403 == false) $_1324923403=array('SU5UUkFO'.'RVR'.'f'.'RURJVElPTg='.'=','WQ==','b'.'WF'.'p'.'bg==',''.'fmN'.'w'.'Zl9'.'tY'.'XBfdmF'.'s'.'dW'.'U=','',''.'ZQ==',''.'Zg='.'=',''.'ZQ==',''.'Rg'.'==','WA='.'=','Z'.'g==','bWFpbg==',''.'fmNwZl9tYXB'.'fd'.'m'.'FsdWU=','UG9ydGFs','Rg==',''.'Z'.'Q==','ZQ==','WA==','Rg='.'=','RA'.'==','RA'.'='.'=','b'.'Q==','ZA==','WQ'.'==','Zg==',''.'Zg='.'=','Zg==','Z'.'g'.'==','UG'.'9y'.'dG'.'Fs',''.'Rg==','ZQ==',''.'Z'.'Q'.'='.'=','WA==','R'.'g==','RA==',''.'RA'.'='.'=','b'.'Q==','ZA==','WQ==',''.'bWFpbg==','T24'.'=','U'.'2'.'V'.'0'.'dGlu'.'Z'.'3'.'ND'.'aGFu'.'Z'.'2'.'U=',''.'Zg==','Zg==',''.'Z'.'g'.'==','Zg==','b'.'WFpbg==','fmNwZl9tYXB'.'fdm'.'Fs'.'dW'.'U=','ZQ'.'='.'=','ZQ==','ZQ'.'==','R'.'A'.'==','ZQ==','ZQ==','Zg='.'=','Z'.'g='.'=',''.'Zg==','ZQ==','bWFpb'.'g='.'=','fmNw'.'Zl9tY'.'XBf'.'dmFsdWU=','ZQ==','Zg='.'=','Zg==','Zg==','Zg==','b'.'W'.'Fpb'.'g'.'==','fmNw'.'Zl9tY'.'XBf'.'dmF'.'sdWU=','ZQ==','Zg='.'=','U'.'G9ydGF'.'s','U'.'G9ydGFs','Z'.'Q==','ZQ==','UG9y'.'dG'.'F'.'s','Rg'.'==','W'.'A='.'=','Rg='.'=',''.'RA='.'=',''.'Z'.'Q='.'=',''.'ZQ'.'==','RA==',''.'bQ==',''.'Z'.'A==','WQ==','ZQ'.'==','W'.'A==','ZQ'.'==','Rg==','ZQ==','RA==','Zg==','Z'.'Q==','RA==','ZQ'.'==','bQ==','ZA'.'==','WQ'.'==','Zg==','Zg='.'=','Zg==','Zg==','Zg==','Zg==','Z'.'g='.'=','Zg==','bW'.'Fpbg==','fmN'.'wZl9tYXBfd'.'mFs'.'dWU=','Z'.'Q'.'==','ZQ==','U'.'G9ydGFs','Rg'.'='.'=','WA==','VFlQRQ==',''.'RE'.'F'.'URQ==','Rk'.'VBVFVS'.'R'.'VM=','RVhQSVJFRA'.'==','VFlQRQ==',''.'RA==','V'.'FJZ'.'X0RBWVNfQ'.'09VTl'.'Q'.'=','REFURQ'.'==','VF'.'J'.'ZX'.'0RBWVNfQ'.'09VTl'.'Q=','R'.'VhQ'.'SVJFRA'.'='.'=','RkV'.'BVFVSRVM=','Zg='.'=','Zg==','RE9DVU1FTlRfUk'.'9P'.'VA'.'==','L2J'.'pdHJpe'.'C9tb'.'2'.'R1b'.'GVzLw==',''.'L2luc'.'3Rh'.'bGwv'.'aW5kZXgu'.'c'.'Ghw','Lg='.'=','X'.'w'.'==',''.'c'.'2'.'Vhcm'.'No','Tg'.'='.'=','','','QUNUSVZF','WQ==',''.'c29j'.'a'.'WFsbmV0d29yaw'.'==','YWx'.'sb3'.'d'.'fZnJ'.'pZWx'.'kcw='.'=','WQ==','S'.'UQ'.'=',''.'c29'.'ja'.'WFsbmV0d2'.'9yaw==','YWx'.'sb3dfZnJpZW'.'xkcw==','SUQ'.'=','c29jaWFs'.'bmV0d29yaw='.'=','YWxsb3d'.'fZnJ'.'pZWxkcw==','Tg==','','','QUN'.'U'.'SV'.'ZF','WQ'.'==','c29ja'.'W'.'Fsbm'.'V'.'0d29yaw==','YWxsb'.'3dfbWljcm9ib'.'G9nX'.'3VzZXI=',''.'W'.'Q'.'==','SU'.'Q=','c29jaWF'.'sbmV0d29yaw'.'==','YW'.'xsb3d'.'fbWljc'.'m9i'.'bG9n'.'X3VzZ'.'XI=',''.'SUQ'.'=','c29jaWFs'.'bm'.'V0d29yaw==',''.'YWxs'.'b3df'.'b'.'Wljcm9ibG9nX3VzZXI'.'=',''.'c29j'.'a'.'WFsbmV0d29yaw==',''.'YWxsb3dfbWlj'.'cm9ibG9'.'nX2dy'.'b'.'3Vw','WQ='.'=','S'.'U'.'Q'.'=','c'.'29jaWFsb'.'mV0d29yaw'.'==','Y'.'Wxsb3dfbWljc'.'m9ib'.'G'.'9nX2d'.'yb'.'3'.'Vw','S'.'UQ=','c29'.'j'.'aWFsbmV0'.'d29'.'yaw==','Y'.'Wxsb3dfb'.'Wljc'.'m9i'.'b'.'G9nX'.'2dyb3Vw','T'.'g==','','','QUNUSVZF',''.'WQ='.'=','c29'.'j'.'aWFsbmV0d2'.'9y'.'aw==','YW'.'xsb'.'3df'.'Zm'.'lsZXNfdXNlcg'.'==','WQ'.'==','SUQ=',''.'c'.'29jaWFsb'.'mV0d29y'.'a'.'w==','YWx'.'sb3dfZ'.'mlsZXNfdXNlcg==','SU'.'Q'.'=','c29'.'jaW'.'FsbmV0'.'d29'.'yaw==','Y'.'W'.'xsb3dfZmls'.'Z'.'XNfd'.'X'.'Nlc'.'g'.'==','T'.'g==','','','QUNUSVZF','WQ='.'=','c'.'29'.'jaWFs'.'bmV0d29'.'y'.'aw='.'=','Y'.'Wx'.'sb'.'3d'.'fY'.'m'.'xvZ1'.'91'.'c2V'.'y','WQ==','SUQ'.'=','c29j'.'aWFsbm'.'V0d'.'29yaw==','YWxs'.'b3'.'dfYmxvZ'.'191'.'c2Vy','SUQ=','c29j'.'aWFsbmV0d'.'2'.'9yaw'.'==','Y'.'Wxsb3dfYm'.'xvZ191c2Vy','Tg==','','','QUNUSVZF','WQ==','c29ja'.'WFsbmV0'.'d29yaw'.'==','YWxs'.'b3dfc'.'GhvdG9fdXNlcg==','W'.'Q==','SUQ=','c'.'29jaW'.'F'.'sbmV'.'0'.'d29yaw'.'==','YWxsb3dfcGhvdG9fdXN'.'lc'.'g==','S'.'UQ=','c29'.'jaWFsbmV0d'.'29yaw==',''.'YWxsb3'.'dfc'.'Gh'.'vdG'.'9fdXNlcg==','Tg==','','','Q'.'UNUS'.'VZF','WQ'.'==','c29jaWFsbmV0'.'d29'.'yaw'.'==','Y'.'Wx'.'sb3dfZm'.'9ydW1f'.'dX'.'Nlc'.'g'.'==','WQ==','SUQ=','c'.'29j'.'aWFsbmV0d'.'29yaw'.'==','YW'.'xsb3dfZm9ydW1f'.'dXNlc'.'g==',''.'SUQ=','c29j'.'aWFsbmV'.'0d2'.'9y'.'aw==','YWxsb'.'3'.'d'.'fZm9y'.'dW1fdXNl'.'cg==','Tg'.'==','','','QUNUSVZF','W'.'Q==','c29jaWF'.'sbmV0d29yaw==','YW'.'xsb3'.'d'.'fdGFza3NfdX'.'N'.'lcg==','WQ'.'==','SU'.'Q=','c29j'.'aWFsbmV0d29y'.'aw==','YWxsb3df'.'d'.'GFza'.'3NfdXNlcg==','S'.'UQ=','c29jaWF'.'sbmV0d29yaw==','YWxsb3dfdGFza3N'.'fd'.'XN'.'lcg==','c29jaW'.'Fsb'.'mV0d29'.'yaw==','YWxsb'.'3dfdGFz'.'a3NfZ'.'3JvdXA=',''.'W'.'Q='.'=',''.'SUQ=','c29ja'.'WFsbmV'.'0d2'.'9yaw==','YWxsb3'.'dfdGFza3NfZ3JvdXA=','SUQ=',''.'c29j'.'aWFs'.'b'.'m'.'V0d29yaw'.'==','YWxsb3'.'d'.'fdGFza3N'.'f'.'Z3J'.'vdXA=','dGFza3M=',''.'Tg==','','','QUNUSV'.'Z'.'F','WQ==',''.'c'.'29ja'.'WFsbm'.'V'.'0'.'d'.'29'.'yaw='.'=',''.'YWxsb3dfY'.'2FsZ'.'W5kYXJfdXNlcg'.'==','W'.'Q==','SUQ=',''.'c29jaWF'.'sbmV0d29y'.'a'.'w==','YWx'.'sb3dfY2FsZW5kYXJfdX'.'N'.'lcg==','SUQ=',''.'c29jaWFs'.'bmV0d2'.'9yaw==','YWxsb'.'3'.'d'.'fY2FsZW5kY'.'XJfdXNlcg'.'==','c2'.'9'.'jaWFs'.'bmV0d29yaw'.'==','YWxs'.'b'.'3dfY'.'2FsZW'.'5kYXJf'.'Z'.'3J'.'vd'.'XA=','WQ='.'=',''.'S'.'UQ=','c29'.'j'.'aWFsbmV0d29yaw'.'==','YWxsb3df'.'Y'.'2FsZ'.'W5kYXJfZ'.'3JvdXA'.'=','S'.'UQ=',''.'c'.'2'.'9jaWFsbmV0d29yaw'.'==','YW'.'xsb3df'.'Y2FsZW5kYXJfZ3Jvd'.'XA=','Y2Fs'.'ZW5'.'k'.'YXI=','QUNU'.'SVZF','WQ==','Tg==','Z'.'Xh0cmFuZXQ=',''.'aWJsb2N'.'r','T25BZnRl'.'cklCbG9ja'.'0VsZ'.'W1lbnRVcG'.'RhdGU'.'=','aW50cmFuZXQ=','Q0lud'.'HJ'.'hbmV'.'0RX'.'ZlbnRIYW5kbGVy'.'cw==',''.'U1'.'BSZWdp'.'c3RlclV'.'wZGF0'.'ZWRJd'.'GVt','Q0'.'ludHJhbm'.'V0U2h'.'h'.'c'.'mVwb2lu'.'d'.'Do6QW'.'dlb'.'nRMaXN0cygpOw==','aW50cmFu'.'ZX'.'Q=','T'.'g==',''.'Q0ludHJhbmV0'.'U2h'.'hc'.'mVwb2l'.'udD'.'o6QWdl'.'bnRRdWV'.'1ZSgpOw'.'='.'=','aW50cmFuZXQ=','Tg==','Q0ludHJhbmV0'.'U2hhc'.'mVw'.'b2lu'.'dDo6QW'.'dlbnRVcGR'.'hdGUoK'.'Ts=','a'.'W50cmFuZXQ=',''.'Tg==','aW'.'Jsb2N'.'r',''.'T25BZn'.'Rl'.'c'.'klC'.'bG9'.'ja0VsZW1lb'.'nRBZG'.'Q=','aW'.'50c'.'m'.'Fu'.'ZXQ=','Q0ludHJhbmV'.'0R'.'XZlbnRI'.'YW5k'.'b'.'GV'.'ycw='.'=','U1BS'.'ZW'.'d'.'pc3RlclV'.'wZGF0ZWRJ'.'d'.'GVt','aWJsb2Nr','T'.'25'.'B'.'ZnR'.'l'.'ck'.'l'.'Cb'.'G9ja'.'0VsZW1l'.'bnRV'.'cGRhdG'.'U'.'=','aW5'.'0cmFuZX'.'Q=',''.'Q0lud'.'H'.'Jhb'.'mV0'.'RXZlbnRIY'.'W'.'5kbGVycw==','U1BS'.'ZWdpc3RlclV'.'wZGF0ZWRJ'.'dGV'.'t','Q0lud'.'HJhbmV0U2hhcmVw'.'b'.'2ludDo'.'6'.'QWdl'.'bnRMa'.'XN0cy'.'g'.'pOw==','aW50c'.'mFuZXQ=','Q0'.'l'.'udHJhb'.'mV'.'0U2hhcmVwb2ludDo6QWdl'.'bnRRdWV1ZS'.'gpOw==','a'.'W'.'50cmFu'.'Z'.'XQ=','Q0ludHJ'.'hbmV'.'0U2hhcmVwb2'.'l'.'udDo6QWd'.'lbn'.'RVcG'.'Rh'.'dGUoKTs=','aW'.'50cmFuZX'.'Q=','Y'.'3J'.'t',''.'bWFpb'.'g==','T25CZWZv'.'cmV'.'Qc'.'m9'.'sb'.'2c=','bW'.'Fpbg==','Q1dpem'.'FyZFNvbFBhbmVs'.'SW'.'50cmF'.'uZXQ=','U2'.'h'.'vd1'.'Bhb'.'mVs','L21'.'vZHVsZXMva'.'W50cmFuZXQv'.'cGFuZWxfYnV0dG9uL'.'nBo'.'cA==','RU5DT0'.'RF','W'.'Q'.'==');return base64_decode($_1324923403[$_1384917633]);}; $GLOBALS['____75015782'][0](___1327645095(0), ___1327645095(1)); class CBXFeatures{ private static $_678636933= 30; private static $_1623714327= array( "Portal" => array( "CompanyCalendar", "CompanyPhoto", "CompanyVideo", "CompanyCareer", "StaffChanges", "StaffAbsence", "CommonDocuments", "MeetingRoomBookingSystem", "Wiki", "Learning", "Vote", "WebLink", "Subscribe", "Friends", "PersonalFiles", "PersonalBlog", "PersonalPhoto", "PersonalForum", "Blog", "Forum", "Gallery", "Board", "MicroBlog", "WebMessenger",), "Communications" => array( "Tasks", "Calendar", "Workgroups", "Jabber", "VideoConference", "Extranet", "SMTP", "Requests", "DAV", "intranet_sharepoint", "timeman", "Idea", "Meeting", "EventList", "Salary", "XDImport",), "Enterprise" => array( "BizProc", "Lists", "Support", "Analytics", "crm", "Controller",), "Holding" => array( "Cluster", "MultiSites",),); private static $_729031245= false; private static $_478111461= false; private static function Initialize(){ if(self::$_729031245 == false){ self::$_729031245= array(); foreach(self::$_1623714327 as $_1885714536 => $_88774608){ foreach($_88774608 as $_29030417) self::$_729031245[$_29030417]= $_1885714536;}} if(self::$_478111461 == false){ self::$_478111461= array(); $_283543233= COption::GetOptionString(___1327645095(2), ___1327645095(3), ___1327645095(4)); if($GLOBALS['____75015782'][1]($_283543233)>(1084/2-542)){ $_283543233= $GLOBALS['____75015782'][2]($_283543233); self::$_478111461= $GLOBALS['____75015782'][3]($_283543233); if(!$GLOBALS['____75015782'][4](self::$_478111461)) self::$_478111461= array();} if($GLOBALS['____75015782'][5](self::$_478111461) <=(936-2*468)) self::$_478111461= array(___1327645095(5) => array(), ___1327645095(6) => array());}} public static function InitiateEditionsSettings($_610384767){ self::Initialize(); $_154141223= array(); foreach(self::$_1623714327 as $_1885714536 => $_88774608){ $_1175608980= $GLOBALS['____75015782'][6]($_1885714536, $_610384767); self::$_478111461[___1327645095(7)][$_1885714536]=($_1175608980? array(___1327645095(8)): array(___1327645095(9))); foreach($_88774608 as $_29030417){ self::$_478111461[___1327645095(10)][$_29030417]= $_1175608980; if(!$_1175608980) $_154141223[]= array($_29030417, false);}} $_1461461210= $GLOBALS['____75015782'][7](self::$_478111461); $_1461461210= $GLOBALS['____75015782'][8]($_1461461210); COption::SetOptionString(___1327645095(11), ___1327645095(12), $_1461461210); foreach($_154141223 as $_1009078527) self::ExecuteEvent($_1009078527[(139*2-278)], $_1009078527[round(0+0.2+0.2+0.2+0.2+0.2)]);} public static function IsFeatureEnabled($_29030417){ if($GLOBALS['____75015782'][9]($_29030417) <= 0) return true; self::Initialize(); if(!$GLOBALS['____75015782'][10]($_29030417, self::$_729031245)) return true; if(self::$_729031245[$_29030417] == ___1327645095(13)) $_1972128148= array(___1327645095(14)); elseif($GLOBALS['____75015782'][11](self::$_729031245[$_29030417], self::$_478111461[___1327645095(15)])) $_1972128148= self::$_478111461[___1327645095(16)][self::$_729031245[$_29030417]]; else $_1972128148= array(___1327645095(17)); if($_1972128148[(1144/2-572)] != ___1327645095(18) && $_1972128148[min(234,0,78)] != ___1327645095(19)){ return false;} elseif($_1972128148[min(112,0,37.333333333333)] == ___1327645095(20)){ if($_1972128148[round(0+0.5+0.5)]< $GLOBALS['____75015782'][12]((1368/2-684), min(194,0,64.666666666667), min(100,0,33.333333333333), Date(___1327645095(21)), $GLOBALS['____75015782'][13](___1327645095(22))- self::$_678636933, $GLOBALS['____75015782'][14](___1327645095(23)))){ if(!isset($_1972128148[round(0+0.5+0.5+0.5+0.5)]) ||!$_1972128148[round(0+1+1)]) self::MarkTrialPeriodExpired(self::$_729031245[$_29030417]); return false;}} return!$GLOBALS['____75015782'][15]($_29030417, self::$_478111461[___1327645095(24)]) || self::$_478111461[___1327645095(25)][$_29030417];} public static function IsFeatureInstalled($_29030417){ if($GLOBALS['____75015782'][16]($_29030417) <= 0) return true; self::Initialize(); return($GLOBALS['____75015782'][17]($_29030417, self::$_478111461[___1327645095(26)]) && self::$_478111461[___1327645095(27)][$_29030417]);} public static function IsFeatureEditable($_29030417){ if($GLOBALS['____75015782'][18]($_29030417) <= 0) return true; self::Initialize(); if(!$GLOBALS['____75015782'][19]($_29030417, self::$_729031245)) return true; if(self::$_729031245[$_29030417] == ___1327645095(28)) $_1972128148= array(___1327645095(29)); elseif($GLOBALS['____75015782'][20](self::$_729031245[$_29030417], self::$_478111461[___1327645095(30)])) $_1972128148= self::$_478111461[___1327645095(31)][self::$_729031245[$_29030417]]; else $_1972128148= array(___1327645095(32)); if($_1972128148[min(76,0,25.333333333333)] != ___1327645095(33) && $_1972128148[(1060/2-530)] != ___1327645095(34)){ return false;} elseif($_1972128148[(970-2*485)] == ___1327645095(35)){ if($_1972128148[round(0+0.33333333333333+0.33333333333333+0.33333333333333)]< $GLOBALS['____75015782'][21](min(108,0,36),(852-2*426), min(50,0,16.666666666667), Date(___1327645095(36)), $GLOBALS['____75015782'][22](___1327645095(37))- self::$_678636933, $GLOBALS['____75015782'][23](___1327645095(38)))){ if(!isset($_1972128148[round(0+1+1)]) ||!$_1972128148[round(0+0.5+0.5+0.5+0.5)]) self::MarkTrialPeriodExpired(self::$_729031245[$_29030417]); return false;}} return true;} private static function ExecuteEvent($_29030417, $_860968424){ if($GLOBALS['____75015782'][24]("CBXFeatures", "On".$_29030417."SettingsChange")) $GLOBALS['____75015782'][25](array("CBXFeatures", "On".$_29030417."SettingsChange"), array($_29030417, $_860968424)); $_49089308= $GLOBALS['_____964133203'][0](___1327645095(39), ___1327645095(40).$_29030417.___1327645095(41)); while($_1357509974= $_49089308->Fetch()) $GLOBALS['_____964133203'][1]($_1357509974, array($_29030417, $_860968424));} public static function SetFeatureEnabled($_29030417, $_860968424= true, $_920950066= true){ if($GLOBALS['____75015782'][26]($_29030417) <= 0) return; if(!self::IsFeatureEditable($_29030417)) $_860968424= false; $_860968424=($_860968424? true: false); self::Initialize(); $_1674344325=(!$GLOBALS['____75015782'][27]($_29030417, self::$_478111461[___1327645095(42)]) && $_860968424 || $GLOBALS['____75015782'][28]($_29030417, self::$_478111461[___1327645095(43)]) && $_860968424 != self::$_478111461[___1327645095(44)][$_29030417]); self::$_478111461[___1327645095(45)][$_29030417]= $_860968424; $_1461461210= $GLOBALS['____75015782'][29](self::$_478111461); $_1461461210= $GLOBALS['____75015782'][30]($_1461461210); COption::SetOptionString(___1327645095(46), ___1327645095(47), $_1461461210); if($_1674344325 && $_920950066) self::ExecuteEvent($_29030417, $_860968424);} private static function MarkTrialPeriodExpired($_1885714536){ if($GLOBALS['____75015782'][31]($_1885714536) <= 0 || $_1885714536 == "Portal") return; self::Initialize(); if(!$GLOBALS['____75015782'][32]($_1885714536, self::$_478111461[___1327645095(48)]) || $GLOBALS['____75015782'][33]($_1885714536, self::$_478111461[___1327645095(49)]) && self::$_478111461[___1327645095(50)][$_1885714536][(1436/2-718)] != ___1327645095(51)) return; if(isset(self::$_478111461[___1327645095(52)][$_1885714536][round(0+1+1)]) && self::$_478111461[___1327645095(53)][$_1885714536][round(0+0.5+0.5+0.5+0.5)]) return; $_154141223= array(); if($GLOBALS['____75015782'][34]($_1885714536, self::$_1623714327) && $GLOBALS['____75015782'][35](self::$_1623714327[$_1885714536])){ foreach(self::$_1623714327[$_1885714536] as $_29030417){ if($GLOBALS['____75015782'][36]($_29030417, self::$_478111461[___1327645095(54)]) && self::$_478111461[___1327645095(55)][$_29030417]){ self::$_478111461[___1327645095(56)][$_29030417]= false; $_154141223[]= array($_29030417, false);}} self::$_478111461[___1327645095(57)][$_1885714536][round(0+0.66666666666667+0.66666666666667+0.66666666666667)]= true;} $_1461461210= $GLOBALS['____75015782'][37](self::$_478111461); $_1461461210= $GLOBALS['____75015782'][38]($_1461461210); COption::SetOptionString(___1327645095(58), ___1327645095(59), $_1461461210); foreach($_154141223 as $_1009078527) self::ExecuteEvent($_1009078527[(218*2-436)], $_1009078527[round(0+0.25+0.25+0.25+0.25)]);} public static function ModifyFeaturesSettings($_610384767, $_88774608){ self::Initialize(); foreach($_610384767 as $_1885714536 => $_278948033) self::$_478111461[___1327645095(60)][$_1885714536]= $_278948033; $_154141223= array(); foreach($_88774608 as $_29030417 => $_860968424){ if(!$GLOBALS['____75015782'][39]($_29030417, self::$_478111461[___1327645095(61)]) && $_860968424 || $GLOBALS['____75015782'][40]($_29030417, self::$_478111461[___1327645095(62)]) && $_860968424 != self::$_478111461[___1327645095(63)][$_29030417]) $_154141223[]= array($_29030417, $_860968424); self::$_478111461[___1327645095(64)][$_29030417]= $_860968424;} $_1461461210= $GLOBALS['____75015782'][41](self::$_478111461); $_1461461210= $GLOBALS['____75015782'][42]($_1461461210); COption::SetOptionString(___1327645095(65), ___1327645095(66), $_1461461210); self::$_478111461= false; foreach($_154141223 as $_1009078527) self::ExecuteEvent($_1009078527[(1064/2-532)], $_1009078527[round(0+1)]);} public static function SaveFeaturesSettings($_931397077, $_400253223){ self::Initialize(); $_1118922867= array(___1327645095(67) => array(), ___1327645095(68) => array()); if(!$GLOBALS['____75015782'][43]($_931397077)) $_931397077= array(); if(!$GLOBALS['____75015782'][44]($_400253223)) $_400253223= array(); if(!$GLOBALS['____75015782'][45](___1327645095(69), $_931397077)) $_931397077[]= ___1327645095(70); foreach(self::$_1623714327 as $_1885714536 => $_88774608){ if($GLOBALS['____75015782'][46]($_1885714536, self::$_478111461[___1327645095(71)])) $_1880022999= self::$_478111461[___1327645095(72)][$_1885714536]; else $_1880022999=($_1885714536 == ___1327645095(73))? array(___1327645095(74)): array(___1327645095(75)); if($_1880022999[(1228/2-614)] == ___1327645095(76) || $_1880022999[(1288/2-644)] == ___1327645095(77)){ $_1118922867[___1327645095(78)][$_1885714536]= $_1880022999;} else{ if($GLOBALS['____75015782'][47]($_1885714536, $_931397077)) $_1118922867[___1327645095(79)][$_1885714536]= array(___1327645095(80), $GLOBALS['____75015782'][48]((838-2*419),(237*2-474),(1496/2-748), $GLOBALS['____75015782'][49](___1327645095(81)), $GLOBALS['____75015782'][50](___1327645095(82)), $GLOBALS['____75015782'][51](___1327645095(83)))); else $_1118922867[___1327645095(84)][$_1885714536]= array(___1327645095(85));}} $_154141223= array(); foreach(self::$_729031245 as $_29030417 => $_1885714536){ if($_1118922867[___1327645095(86)][$_1885714536][(240*2-480)] != ___1327645095(87) && $_1118922867[___1327645095(88)][$_1885714536][(151*2-302)] != ___1327645095(89)){ $_1118922867[___1327645095(90)][$_29030417]= false;} else{ if($_1118922867[___1327645095(91)][$_1885714536][(145*2-290)] == ___1327645095(92) && $_1118922867[___1327645095(93)][$_1885714536][round(0+0.33333333333333+0.33333333333333+0.33333333333333)]< $GLOBALS['____75015782'][52]((1368/2-684),(189*2-378),(208*2-416), Date(___1327645095(94)), $GLOBALS['____75015782'][53](___1327645095(95))- self::$_678636933, $GLOBALS['____75015782'][54](___1327645095(96)))) $_1118922867[___1327645095(97)][$_29030417]= false; else $_1118922867[___1327645095(98)][$_29030417]= $GLOBALS['____75015782'][55]($_29030417, $_400253223); if(!$GLOBALS['____75015782'][56]($_29030417, self::$_478111461[___1327645095(99)]) && $_1118922867[___1327645095(100)][$_29030417] || $GLOBALS['____75015782'][57]($_29030417, self::$_478111461[___1327645095(101)]) && $_1118922867[___1327645095(102)][$_29030417] != self::$_478111461[___1327645095(103)][$_29030417]) $_154141223[]= array($_29030417, $_1118922867[___1327645095(104)][$_29030417]);}} $_1461461210= $GLOBALS['____75015782'][58]($_1118922867); $_1461461210= $GLOBALS['____75015782'][59]($_1461461210); COption::SetOptionString(___1327645095(105), ___1327645095(106), $_1461461210); self::$_478111461= false; foreach($_154141223 as $_1009078527) self::ExecuteEvent($_1009078527[(868-2*434)], $_1009078527[round(0+0.5+0.5)]);} public static function GetFeaturesList(){ self::Initialize(); $_1456321050= array(); foreach(self::$_1623714327 as $_1885714536 => $_88774608){ if($GLOBALS['____75015782'][60]($_1885714536, self::$_478111461[___1327645095(107)])) $_1880022999= self::$_478111461[___1327645095(108)][$_1885714536]; else $_1880022999=($_1885714536 == ___1327645095(109))? array(___1327645095(110)): array(___1327645095(111)); $_1456321050[$_1885714536]= array( ___1327645095(112) => $_1880022999[(229*2-458)], ___1327645095(113) => $_1880022999[round(0+0.33333333333333+0.33333333333333+0.33333333333333)], ___1327645095(114) => array(),); $_1456321050[$_1885714536][___1327645095(115)]= false; if($_1456321050[$_1885714536][___1327645095(116)] == ___1327645095(117)){ $_1456321050[$_1885714536][___1327645095(118)]= $GLOBALS['____75015782'][61](($GLOBALS['____75015782'][62]()- $_1456321050[$_1885714536][___1327645095(119)])/ round(0+43200+43200)); if($_1456321050[$_1885714536][___1327645095(120)]> self::$_678636933) $_1456321050[$_1885714536][___1327645095(121)]= true;} foreach($_88774608 as $_29030417) $_1456321050[$_1885714536][___1327645095(122)][$_29030417]=(!$GLOBALS['____75015782'][63]($_29030417, self::$_478111461[___1327645095(123)]) || self::$_478111461[___1327645095(124)][$_29030417]);} return $_1456321050;} private static function InstallModule($_1447159094, $_1356220819){ if(IsModuleInstalled($_1447159094) == $_1356220819) return true; $_669442584= $_SERVER[___1327645095(125)].___1327645095(126).$_1447159094.___1327645095(127); if(!$GLOBALS['____75015782'][64]($_669442584)) return false; include_once($_669442584); $_1976641162= $GLOBALS['____75015782'][65](___1327645095(128), ___1327645095(129), $_1447159094); if(!$GLOBALS['____75015782'][66]($_1976641162)) return false; $_168067938= new $_1976641162; if($_1356220819){ if(!$_168067938->InstallDB()) return false; $_168067938->InstallEvents(); if(!$_168067938->InstallFiles()) return false;} else{ if(CModule::IncludeModule(___1327645095(130))) CSearch::DeleteIndex($_1447159094); UnRegisterModule($_1447159094);     } return true;} private static function OnRequestsSettingsChange($_29030417, $_860968424){ self::InstallModule("form", $_860968424);} private static function OnLearningSettingsChange($_29030417, $_860968424){ self::InstallModule("learning", $_860968424);} private static function OnJabberSettingsChange($_29030417, $_860968424){ self::InstallModule("xmpp", $_860968424);} private static function OnVideoConferenceSettingsChange($_29030417, $_860968424){ self::InstallModule("video", $_860968424);} private static function OnBizProcSettingsChange($_29030417, $_860968424){ self::InstallModule("bizprocdesigner", $_860968424);} private static function OnListsSettingsChange($_29030417, $_860968424){ self::InstallModule("lists", $_860968424);} private static function OnWikiSettingsChange($_29030417, $_860968424){ self::InstallModule("wiki", $_860968424);} private static function OnSupportSettingsChange($_29030417, $_860968424){ self::InstallModule("support", $_860968424);} private static function OnControllerSettingsChange($_29030417, $_860968424){ self::InstallModule("controller", $_860968424);} private static function OnAnalyticsSettingsChange($_29030417, $_860968424){ self::InstallModule("statistic", $_860968424);} private static function OnVoteSettingsChange($_29030417, $_860968424){ self::InstallModule("vote", $_860968424);} private static function OnFriendsSettingsChange($_29030417, $_860968424){ if($_860968424) $_479585328= "Y"; else $_479585328= ___1327645095(131); $_1498706952= CSite::GetList(($_1175608980= ___1327645095(132)),($_68103592= ___1327645095(133)), array(___1327645095(134) => ___1327645095(135))); while($_638365264= $_1498706952->Fetch()){ if(COption::GetOptionString(___1327645095(136), ___1327645095(137), ___1327645095(138), $_638365264[___1327645095(139)]) != $_479585328){ COption::SetOptionString(___1327645095(140), ___1327645095(141), $_479585328, false, $_638365264[___1327645095(142)]); COption::SetOptionString(___1327645095(143), ___1327645095(144), $_479585328);}}} private static function OnMicroBlogSettingsChange($_29030417, $_860968424){ if($_860968424) $_479585328= "Y"; else $_479585328= ___1327645095(145); $_1498706952= CSite::GetList(($_1175608980= ___1327645095(146)),($_68103592= ___1327645095(147)), array(___1327645095(148) => ___1327645095(149))); while($_638365264= $_1498706952->Fetch()){ if(COption::GetOptionString(___1327645095(150), ___1327645095(151), ___1327645095(152), $_638365264[___1327645095(153)]) != $_479585328){ COption::SetOptionString(___1327645095(154), ___1327645095(155), $_479585328, false, $_638365264[___1327645095(156)]); COption::SetOptionString(___1327645095(157), ___1327645095(158), $_479585328);} if(COption::GetOptionString(___1327645095(159), ___1327645095(160), ___1327645095(161), $_638365264[___1327645095(162)]) != $_479585328){ COption::SetOptionString(___1327645095(163), ___1327645095(164), $_479585328, false, $_638365264[___1327645095(165)]); COption::SetOptionString(___1327645095(166), ___1327645095(167), $_479585328);}}} private static function OnPersonalFilesSettingsChange($_29030417, $_860968424){ if($_860968424) $_479585328= "Y"; else $_479585328= ___1327645095(168); $_1498706952= CSite::GetList(($_1175608980= ___1327645095(169)),($_68103592= ___1327645095(170)), array(___1327645095(171) => ___1327645095(172))); while($_638365264= $_1498706952->Fetch()){ if(COption::GetOptionString(___1327645095(173), ___1327645095(174), ___1327645095(175), $_638365264[___1327645095(176)]) != $_479585328){ COption::SetOptionString(___1327645095(177), ___1327645095(178), $_479585328, false, $_638365264[___1327645095(179)]); COption::SetOptionString(___1327645095(180), ___1327645095(181), $_479585328);}}} private static function OnPersonalBlogSettingsChange($_29030417, $_860968424){ if($_860968424) $_479585328= "Y"; else $_479585328= ___1327645095(182); $_1498706952= CSite::GetList(($_1175608980= ___1327645095(183)),($_68103592= ___1327645095(184)), array(___1327645095(185) => ___1327645095(186))); while($_638365264= $_1498706952->Fetch()){ if(COption::GetOptionString(___1327645095(187), ___1327645095(188), ___1327645095(189), $_638365264[___1327645095(190)]) != $_479585328){ COption::SetOptionString(___1327645095(191), ___1327645095(192), $_479585328, false, $_638365264[___1327645095(193)]); COption::SetOptionString(___1327645095(194), ___1327645095(195), $_479585328);}}} private static function OnPersonalPhotoSettingsChange($_29030417, $_860968424){ if($_860968424) $_479585328= "Y"; else $_479585328= ___1327645095(196); $_1498706952= CSite::GetList(($_1175608980= ___1327645095(197)),($_68103592= ___1327645095(198)), array(___1327645095(199) => ___1327645095(200))); while($_638365264= $_1498706952->Fetch()){ if(COption::GetOptionString(___1327645095(201), ___1327645095(202), ___1327645095(203), $_638365264[___1327645095(204)]) != $_479585328){ COption::SetOptionString(___1327645095(205), ___1327645095(206), $_479585328, false, $_638365264[___1327645095(207)]); COption::SetOptionString(___1327645095(208), ___1327645095(209), $_479585328);}}} private static function OnPersonalForumSettingsChange($_29030417, $_860968424){ if($_860968424) $_479585328= "Y"; else $_479585328= ___1327645095(210); $_1498706952= CSite::GetList(($_1175608980= ___1327645095(211)),($_68103592= ___1327645095(212)), array(___1327645095(213) => ___1327645095(214))); while($_638365264= $_1498706952->Fetch()){ if(COption::GetOptionString(___1327645095(215), ___1327645095(216), ___1327645095(217), $_638365264[___1327645095(218)]) != $_479585328){ COption::SetOptionString(___1327645095(219), ___1327645095(220), $_479585328, false, $_638365264[___1327645095(221)]); COption::SetOptionString(___1327645095(222), ___1327645095(223), $_479585328);}}} private static function OnTasksSettingsChange($_29030417, $_860968424){ if($_860968424) $_479585328= "Y"; else $_479585328= ___1327645095(224); $_1498706952= CSite::GetList(($_1175608980= ___1327645095(225)),($_68103592= ___1327645095(226)), array(___1327645095(227) => ___1327645095(228))); while($_638365264= $_1498706952->Fetch()){ if(COption::GetOptionString(___1327645095(229), ___1327645095(230), ___1327645095(231), $_638365264[___1327645095(232)]) != $_479585328){ COption::SetOptionString(___1327645095(233), ___1327645095(234), $_479585328, false, $_638365264[___1327645095(235)]); COption::SetOptionString(___1327645095(236), ___1327645095(237), $_479585328);} if(COption::GetOptionString(___1327645095(238), ___1327645095(239), ___1327645095(240), $_638365264[___1327645095(241)]) != $_479585328){ COption::SetOptionString(___1327645095(242), ___1327645095(243), $_479585328, false, $_638365264[___1327645095(244)]); COption::SetOptionString(___1327645095(245), ___1327645095(246), $_479585328);}} self::InstallModule(___1327645095(247), $_860968424);} private static function OnCalendarSettingsChange($_29030417, $_860968424){ if($_860968424) $_479585328= "Y"; else $_479585328= ___1327645095(248); $_1498706952= CSite::GetList(($_1175608980= ___1327645095(249)),($_68103592= ___1327645095(250)), array(___1327645095(251) => ___1327645095(252))); while($_638365264= $_1498706952->Fetch()){ if(COption::GetOptionString(___1327645095(253), ___1327645095(254), ___1327645095(255), $_638365264[___1327645095(256)]) != $_479585328){ COption::SetOptionString(___1327645095(257), ___1327645095(258), $_479585328, false, $_638365264[___1327645095(259)]); COption::SetOptionString(___1327645095(260), ___1327645095(261), $_479585328);} if(COption::GetOptionString(___1327645095(262), ___1327645095(263), ___1327645095(264), $_638365264[___1327645095(265)]) != $_479585328){ COption::SetOptionString(___1327645095(266), ___1327645095(267), $_479585328, false, $_638365264[___1327645095(268)]); COption::SetOptionString(___1327645095(269), ___1327645095(270), $_479585328);}} self::InstallModule(___1327645095(271), $_860968424);} private static function OnSMTPSettingsChange($_29030417, $_860968424){ self::InstallModule("mail", $_860968424);} private static function OnExtranetSettingsChange($_29030417, $_860968424){ $_1242586809= COption::GetOptionString("extranet", "extranet_site", ""); if($_1242586809){ $_773044028= new CSite; $_773044028->Update($_1242586809, array(___1327645095(272) =>($_860968424? ___1327645095(273): ___1327645095(274))));} self::InstallModule(___1327645095(275), $_860968424);} private static function OnDAVSettingsChange($_29030417, $_860968424){ self::InstallModule("dav", $_860968424);} private static function OntimemanSettingsChange($_29030417, $_860968424){ self::InstallModule("timeman", $_860968424);} private static function Onintranet_sharepointSettingsChange($_29030417, $_860968424){ if($_860968424){ RegisterModuleDependences("iblock", "OnAfterIBlockElementAdd", "intranet", "CIntranetEventHandlers", "SPRegisterUpdatedItem"); RegisterModuleDependences(___1327645095(276), ___1327645095(277), ___1327645095(278), ___1327645095(279), ___1327645095(280)); CAgent::AddAgent(___1327645095(281), ___1327645095(282), ___1327645095(283), round(0+166.66666666667+166.66666666667+166.66666666667)); CAgent::AddAgent(___1327645095(284), ___1327645095(285), ___1327645095(286), round(0+300)); CAgent::AddAgent(___1327645095(287), ___1327645095(288), ___1327645095(289), round(0+1800+1800));} else{ UnRegisterModuleDependences(___1327645095(290), ___1327645095(291), ___1327645095(292), ___1327645095(293), ___1327645095(294)); UnRegisterModuleDependences(___1327645095(295), ___1327645095(296), ___1327645095(297), ___1327645095(298), ___1327645095(299)); CAgent::RemoveAgent(___1327645095(300), ___1327645095(301)); CAgent::RemoveAgent(___1327645095(302), ___1327645095(303)); CAgent::RemoveAgent(___1327645095(304), ___1327645095(305));}} private static function OncrmSettingsChange($_29030417, $_860968424){ if($_860968424) COption::SetOptionString("crm", "form_features", "Y"); self::InstallModule(___1327645095(306), $_860968424);} private static function OnClusterSettingsChange($_29030417, $_860968424){ self::InstallModule("cluster", $_860968424);} private static function OnMultiSitesSettingsChange($_29030417, $_860968424){ if($_860968424) RegisterModuleDependences("main", "OnBeforeProlog", "main", "CWizardSolPanelIntranet", "ShowPanel", 100, "/modules/intranet/panel_button.php"); else UnRegisterModuleDependences(___1327645095(307), ___1327645095(308), ___1327645095(309), ___1327645095(310), ___1327645095(311), ___1327645095(312));} private static function OnIdeaSettingsChange($_29030417, $_860968424){ self::InstallModule("idea", $_860968424);} private static function OnMeetingSettingsChange($_29030417, $_860968424){ self::InstallModule("meeting", $_860968424);} private static function OnXDImportSettingsChange($_29030417, $_860968424){ self::InstallModule("xdimport", $_860968424);}} $GLOBALS['____75015782'][67](___1327645095(313), ___1327645095(314));/**/			//Do not remove this

//component 2.0 template engines
$GLOBALS["arCustomTemplateEngines"] = array();

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/urlrewriter.php");

/**
 * Defined in dbconn.php
 * @param string $DBType
 */

\Bitrix\Main\Loader::registerAutoLoadClasses(
	"main",
	array(
		"CSiteTemplate" => "classes/general/site_template.php",
		"CBitrixComponent" => "classes/general/component.php",
		"CComponentEngine" => "classes/general/component_engine.php",
		"CComponentAjax" => "classes/general/component_ajax.php",
		"CBitrixComponentTemplate" => "classes/general/component_template.php",
		"CComponentUtil" => "classes/general/component_util.php",
		"CControllerClient" => "classes/general/controller_member.php",
		"PHPParser" => "classes/general/php_parser.php",
		"CDiskQuota" => "classes/".$DBType."/quota.php",
		"CEventLog" => "classes/general/event_log.php",
		"CEventMain" => "classes/general/event_log.php",
		"CAdminFileDialog" => "classes/general/file_dialog.php",
		"WLL_User" => "classes/general/liveid.php",
		"WLL_ConsentToken" => "classes/general/liveid.php",
		"WindowsLiveLogin" => "classes/general/liveid.php",
		"CAllFile" => "classes/general/file.php",
		"CFile" => "classes/".$DBType."/file.php",
		"CTempFile" => "classes/general/file_temp.php",
		"CFavorites" => "classes/".$DBType."/favorites.php",
		"CUserOptions" => "classes/general/user_options.php",
		"CGridOptions" => "classes/general/grids.php",
		"CUndo" => "/classes/general/undo.php",
		"CAutoSave" => "/classes/general/undo.php",
		"CRatings" => "classes/".$DBType."/ratings.php",
		"CRatingsComponentsMain" => "classes/".$DBType."/ratings_components.php",
		"CRatingRule" => "classes/general/rating_rule.php",
		"CRatingRulesMain" => "classes/".$DBType."/rating_rules.php",
		"CTopPanel" => "public/top_panel.php",
		"CEditArea" => "public/edit_area.php",
		"CComponentPanel" => "public/edit_area.php",
		"CTextParser" => "classes/general/textparser.php",
		"CPHPCacheFiles" => "classes/general/cache_files.php",
		"CDataXML" => "classes/general/xml.php",
		"CXMLFileStream" => "classes/general/xml.php",
		"CRsaProvider" => "classes/general/rsasecurity.php",
		"CRsaSecurity" => "classes/general/rsasecurity.php",
		"CRsaBcmathProvider" => "classes/general/rsabcmath.php",
		"CRsaOpensslProvider" => "classes/general/rsaopenssl.php",
		"CASNReader" => "classes/general/asn.php",
		"CBXShortUri" => "classes/".$DBType."/short_uri.php",
		"CFinder" => "classes/general/finder.php",
		"CAccess" => "classes/general/access.php",
		"CAuthProvider" => "classes/general/authproviders.php",
		"IProviderInterface" => "classes/general/authproviders.php",
		"CGroupAuthProvider" => "classes/general/authproviders.php",
		"CUserAuthProvider" => "classes/general/authproviders.php",
		"CTableSchema" => "classes/general/table_schema.php",
		"CCSVData" => "classes/general/csv_data.php",
		"CSmile" => "classes/general/smile.php",
		"CSmileGallery" => "classes/general/smile.php",
		"CSmileSet" => "classes/general/smile.php",
		"CGlobalCounter" => "classes/general/global_counter.php",
		"CUserCounter" => "classes/".$DBType."/user_counter.php",
		"CUserCounterPage" => "classes/".$DBType."/user_counter.php",
		"CHotKeys" => "classes/general/hot_keys.php",
		"CHotKeysCode" => "classes/general/hot_keys.php",
		"CBXSanitizer" => "classes/general/sanitizer.php",
		"CBXArchive" => "classes/general/archive.php",
		"CAdminNotify" => "classes/general/admin_notify.php",
		"CBXFavAdmMenu" => "classes/general/favorites.php",
		"CAdminInformer" => "classes/general/admin_informer.php",
		"CSiteCheckerTest" => "classes/general/site_checker.php",
		"CSqlUtil" => "classes/general/sql_util.php",
		"CHTMLPagesCache" => "classes/general/cache_html.php",
		"CFileUploader" => "classes/general/uploader.php",
		"LPA" => "classes/general/lpa.php",
	)
);

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/".$DBType."/agent.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/".$DBType."/user.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/".$DBType."/event.php");
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/menu.php");
AddEventHandler("main", "OnAfterEpilog", array("\\Bitrix\\Main\\Data\\ManagedCache", "finalize"));
require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/".$DBType."/usertype.php");

if(file_exists(($_fname = $_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/classes/general/update_db_updater.php")))
{
	$US_HOST_PROCESS_MAIN = False;
	include($_fname);
}

$GLOBALS["APPLICATION"]->AddJSKernelInfo(
	'main',
	array(
		'/bitrix/js/main/core/core.js', '/bitrix/js/main/core/core_ajax.js', '/bitrix/js/main/json/json2.min.js',
		'/bitrix/js/main/core/core_ls.js', '/bitrix/js/main/core/core_popup.js', '/bitrix/js/main/core/core_tooltip.js',
		'/bitrix/js/main/core/core_date.js','/bitrix/js/main/core/core_timer.js', '/bitrix/js/main/core/core_fx.js',
		'/bitrix/js/main/core/core_window.js', '/bitrix/js/main/core/core_autosave.js', '/bitrix/js/main/rating_like.js',
		'/bitrix/js/main/session.js', '/bitrix/js/main/dd.js', '/bitrix/js/main/utils.js',
		'/bitrix/js/main/core/core_dd.js', '/bitrix/js/main/core/core_webrtc.js'
	)
);


$GLOBALS["APPLICATION"]->AddCSSKernelInfo(
	'main',
	array(
		'/bitrix/js/main/core/css/core.css', '/bitrix/js/main/core/css/core_popup.css',
		'/bitrix/js/main/core/css/core_tooltip.css', '/bitrix/js/main/core/css/core_date.css'
	)
);

//Park core uploader
$GLOBALS["APPLICATION"]->AddJSKernelInfo(
	'coreuploader',
	array(
		'/bitrix/js/main/core/core_uploader/common.js',
		'/bitrix/js/main/core/core_uploader/uploader.js',
		'/bitrix/js/main/core/core_uploader/file.js',
		'/bitrix/js/main/core/core_uploader/queue.js',
	)
);

if(file_exists(($_fname = $_SERVER["DOCUMENT_ROOT"]."/bitrix/init.php")))
	include_once($_fname);

if(($_fname = getLocalPath("php_interface/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(($_fname = getLocalPath("php_interface/".SITE_ID."/init.php", BX_PERSONAL_ROOT)) !== false)
	include_once($_SERVER["DOCUMENT_ROOT"].$_fname);

if(!defined("BX_FILE_PERMISSIONS"))
	define("BX_FILE_PERMISSIONS", 0644);
if(!defined("BX_DIR_PERMISSIONS"))
	define("BX_DIR_PERMISSIONS", 0755);

//global var, is used somewhere
$GLOBALS["sDocPath"] = $GLOBALS["APPLICATION"]->GetCurPage();

if((!(defined("STATISTIC_ONLY") && STATISTIC_ONLY && substr($GLOBALS["APPLICATION"]->GetCurPage(), 0, strlen(BX_ROOT."/admin/"))!=BX_ROOT."/admin/")) && COption::GetOptionString("main", "include_charset", "Y")=="Y" && strlen(LANG_CHARSET)>0)
	header("Content-Type: text/html; charset=".LANG_CHARSET);

if(COption::GetOptionString("main", "set_p3p_header", "Y")=="Y")
	header("P3P: policyref=\"/bitrix/p3p.xml\", CP=\"NON DSP COR CUR ADM DEV PSA PSD OUR UNR BUS UNI COM NAV INT DEM STA\"");

//licence key
$LICENSE_KEY = "";
if(file_exists(($_fname = $_SERVER["DOCUMENT_ROOT"].BX_ROOT."/license_key.php")))
	include($_fname);
if($LICENSE_KEY == "" || strtoupper($LICENSE_KEY) == "DEMO")
	define("LICENSE_KEY", "DEMO");
else
	define("LICENSE_KEY", $LICENSE_KEY);

header("X-Powered-CMS: Bitrix Site Manager (".(LICENSE_KEY == "DEMO"? "DEMO" : md5("BITRIX".LICENSE_KEY."LICENCE")).")");

define("BX_CRONTAB_SUPPORT", defined("BX_CRONTAB"));

if(COption::GetOptionString("main", "check_agents", "Y")=="Y")
{
	define("START_EXEC_AGENTS_1", microtime());
	$GLOBALS["BX_STATE"] = "AG";
	$GLOBALS["DB"]->StartUsingMasterOnly();
	CAgent::CheckAgents();
	$GLOBALS["DB"]->StopUsingMasterOnly();
	define("START_EXEC_AGENTS_2", microtime());
	$GLOBALS["BX_STATE"] = "PB";
}

//session initialization
ini_set("session.cookie_httponly", "1");

if($domain = $GLOBALS["APPLICATION"]->GetCookieDomain())
	ini_set("session.cookie_domain", $domain);

if(COption::GetOptionString("security", "session", "N") === "Y"	&& CModule::IncludeModule("security"))
	CSecuritySession::Init();

session_start();

foreach (GetModuleEvents("main", "OnPageStart", true) as $arEvent)
	ExecuteModuleEventEx($arEvent);

//define global user object
$GLOBALS["USER"] = new CUser;

//session control from group policy
$arPolicy = $GLOBALS["USER"]->GetSecurityPolicy();
$currTime = time();
if(
	(
		//IP address changed
		$_SESSION['SESS_IP']
		&& strlen($arPolicy["SESSION_IP_MASK"])>0
		&& (
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($_SESSION['SESS_IP']))
			!=
			(ip2long($arPolicy["SESSION_IP_MASK"]) & ip2long($_SERVER['REMOTE_ADDR']))
		)
	)
	||
	(
		//session timeout
		$arPolicy["SESSION_TIMEOUT"]>0
		&& $_SESSION['SESS_TIME']>0
		&& $currTime-$arPolicy["SESSION_TIMEOUT"]*60 > $_SESSION['SESS_TIME']
	)
	||
	(
		//session expander control
		isset($_SESSION["BX_SESSION_TERMINATE_TIME"])
		&& $_SESSION["BX_SESSION_TERMINATE_TIME"] > 0
		&& $currTime > $_SESSION["BX_SESSION_TERMINATE_TIME"]
	)
	||
	(
		//signed session
		isset($_SESSION["BX_SESSION_SIGN"])
		&& $_SESSION["BX_SESSION_SIGN"] <> bitrix_sess_sign()
	)
	||
	(
		//session manually expired, e.g. in $User->LoginHitByHash
		isSessionExpired()
	)
)
{
	$_SESSION = array();
	@session_destroy();

	//session_destroy cleans user sesssion handles in some PHP versions
	//see http://bugs.php.net/bug.php?id=32330 discussion
	if(COption::GetOptionString("security", "session", "N") === "Y"	&& CModule::IncludeModule("security"))
		CSecuritySession::Init();

	session_id(md5(uniqid(rand(), true)));
	session_start();
	$GLOBALS["USER"] = new CUser;
}
$_SESSION['SESS_IP'] = $_SERVER['REMOTE_ADDR'];
$_SESSION['SESS_TIME'] = time();
if(!isset($_SESSION["BX_SESSION_SIGN"]))
	$_SESSION["BX_SESSION_SIGN"] = bitrix_sess_sign();

//session control from security module
if(
	(COption::GetOptionString("main", "use_session_id_ttl", "N") == "Y")
	&& (COption::GetOptionInt("main", "session_id_ttl", 0) > 0)
	&& !defined("BX_SESSION_ID_CHANGE")
)
{
	if(!array_key_exists('SESS_ID_TIME', $_SESSION))
	{
		$_SESSION['SESS_ID_TIME'] = $_SESSION['SESS_TIME'];
	}
	elseif(($_SESSION['SESS_ID_TIME'] + COption::GetOptionInt("main", "session_id_ttl")) < $_SESSION['SESS_TIME'])
	{
		if(COption::GetOptionString("security", "session", "N") === "Y" && CModule::IncludeModule("security"))
		{
			CSecuritySession::UpdateSessID();
		}
		else
		{
			session_regenerate_id();
		}
		$_SESSION['SESS_ID_TIME'] = $_SESSION['SESS_TIME'];
	}
}

define("BX_STARTED", true);

if (isset($_SESSION['BX_ADMIN_LOAD_AUTH']))
{
	define('ADMIN_SECTION_LOAD_AUTH', 1);
	unset($_SESSION['BX_ADMIN_LOAD_AUTH']);
}

if(!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true)
{
	$bLogout = isset($_REQUEST["logout"]) && (strtolower($_REQUEST["logout"]) == "yes");

	if($bLogout && $GLOBALS["USER"]->IsAuthorized())
	{
		$GLOBALS["USER"]->Logout();
		LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam('', array('logout')));
	}

	// authorize by cookies
	if(!$GLOBALS["USER"]->IsAuthorized())
	{
		$GLOBALS["USER"]->LoginByCookies();
	}

	$arAuthResult = false;

	//http basic and digest authorization
	if(($httpAuth = $GLOBALS["USER"]->LoginByHttpAuth()) !== null)
	{
		$arAuthResult = $httpAuth;
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}

	//Authorize user from authorization html form
	if(isset($_REQUEST["AUTH_FORM"]) && $_REQUEST["AUTH_FORM"] <> '')
	{
		$bRsaError = false;
		if(COption::GetOptionString('main', 'use_encrypted_auth', 'N') == 'Y')
		{
			//possible encrypted user password
			$sec = new CRsaSecurity();
			if(($arKeys = $sec->LoadKeys()))
			{
				$sec->SetKeys($arKeys);
				$errno = $sec->AcceptFromForm(array('USER_PASSWORD', 'USER_CONFIRM_PASSWORD'));
				if($errno == CRsaSecurity::ERROR_SESS_CHECK)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_sess"), "TYPE"=>"ERROR");
				elseif($errno < 0)
					$arAuthResult = array("MESSAGE"=>GetMessage("main_include_decode_pass_err", array("#ERRCODE#"=>$errno)), "TYPE"=>"ERROR");

				if($errno < 0)
					$bRsaError = true;
			}
		}

		if($bRsaError == false)
		{
			if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
				$USER_LID = LANG;
			else
				$USER_LID = false;

			if($_REQUEST["TYPE"] == "AUTH")
			{
				$arAuthResult = $GLOBALS["USER"]->Login($_REQUEST["USER_LOGIN"], $_REQUEST["USER_PASSWORD"], $_REQUEST["USER_REMEMBER"]);
			}
			elseif($_REQUEST["TYPE"] == "OTP")
			{
				$arAuthResult = $GLOBALS["USER"]->LoginByOtp($_REQUEST["USER_OTP"], $_REQUEST["OTP_REMEMBER"], $_REQUEST["captcha_word"], $_REQUEST["captcha_sid"]);
			}
			elseif($_REQUEST["TYPE"] == "SEND_PWD")
			{
				$arAuthResult = CUser::SendPassword($_REQUEST["USER_LOGIN"], $_REQUEST["USER_EMAIL"], $USER_LID);
			}
			elseif($_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST["TYPE"] == "CHANGE_PWD")
			{
				$arAuthResult = $GLOBALS["USER"]->ChangePassword($_REQUEST["USER_LOGIN"], $_REQUEST["USER_CHECKWORD"], $_REQUEST["USER_PASSWORD"], $_REQUEST["USER_CONFIRM_PASSWORD"], $USER_LID);
			}
			elseif(COption::GetOptionString("main", "new_user_registration", "N") == "Y" && $_SERVER['REQUEST_METHOD'] == 'POST' && $_REQUEST["TYPE"] == "REGISTRATION" && (!defined("ADMIN_SECTION") || ADMIN_SECTION!==true))
			{
				$arAuthResult = $GLOBALS["USER"]->Register($_REQUEST["USER_LOGIN"], $_REQUEST["USER_NAME"], $_REQUEST["USER_LAST_NAME"], $_REQUEST["USER_PASSWORD"], $_REQUEST["USER_CONFIRM_PASSWORD"], $_REQUEST["USER_EMAIL"], $USER_LID, $_REQUEST["captcha_word"], $_REQUEST["captcha_sid"]);
			}

			if($_REQUEST["TYPE"] == "AUTH" || $_REQUEST["TYPE"] == "OTP")
			{
				//special login form in the control panel
				if($arAuthResult === true && defined('ADMIN_SECTION') && ADMIN_SECTION === true)
				{
					//store cookies for next hit (see CMain::GetSpreadCookieHTML())
					$GLOBALS["APPLICATION"]->StoreCookies();
					$_SESSION['BX_ADMIN_LOAD_AUTH'] = true;
					echo '<script type="text/javascript">window.onload=function(){top.BX.AUTHAGENT.setAuthResult(false);};</script>';
					die();
				}
			}
		}
		$GLOBALS["APPLICATION"]->SetAuthResult($arAuthResult);
	}
	elseif(!$GLOBALS["USER"]->IsAuthorized())
	{
		//Authorize by unique URL
		$GLOBALS["USER"]->LoginHitByHash();
	}
}

//application password scope control
if(($applicationID = $GLOBALS["USER"]->GetParam("APPLICATION_ID")) !== null)
{
	$appManager = \Bitrix\Main\Authentication\ApplicationManager::getInstance();
	if($appManager->checkScope($applicationID) !== true)
	{
		$event = new \Bitrix\Main\Event("main", "onApplicationScopeError", Array('APPLICATION_ID' => $applicationID));
		$event->send();

		CHTTP::SetStatus("403 Forbidden");
		die();
	}
}

//define the site template
if(!defined("ADMIN_SECTION") || ADMIN_SECTION !== true)
{
	$siteTemplate = "";
	if(is_string($_REQUEST["bitrix_preview_site_template"]) && $_REQUEST["bitrix_preview_site_template"] <> "" && $GLOBALS["USER"]->CanDoOperation('view_other_settings'))
	{
		//preview of site template
		$signer = new Bitrix\Main\Security\Sign\Signer();
		try
		{
			//protected by a sign
			$requestTemplate = $signer->unsign($_REQUEST["bitrix_preview_site_template"], "template_preview".bitrix_sessid());

			$aTemplates = CSiteTemplate::GetByID($requestTemplate);
			if($template = $aTemplates->Fetch())
			{
				$siteTemplate = $template["ID"];

				//preview of unsaved template
				if(isset($_GET['bx_template_preview_mode']) && $_GET['bx_template_preview_mode'] == 'Y' && $GLOBALS["USER"]->CanDoOperation('edit_other_settings'))
				{
					define("SITE_TEMPLATE_PREVIEW_MODE", true);
				}
			}
		}
		catch(\Bitrix\Main\Security\Sign\BadSignatureException $e)
		{
		}
	}
	if($siteTemplate == "")
	{
		$siteTemplate = CSite::GetCurTemplate();
	}
	define("SITE_TEMPLATE_ID", $siteTemplate);
	define("SITE_TEMPLATE_PATH", getLocalPath('templates/'.SITE_TEMPLATE_ID, BX_PERSONAL_ROOT));
}

//magic parameters: show page creation time
if(isset($_GET["show_page_exec_time"]))
{
	if($_GET["show_page_exec_time"]=="Y" || $_GET["show_page_exec_time"]=="N")
		$_SESSION["SESS_SHOW_TIME_EXEC"] = $_GET["show_page_exec_time"];
}

//magic parameters: show included file processing time
if(isset($_GET["show_include_exec_time"]))
{
	if($_GET["show_include_exec_time"]=="Y" || $_GET["show_include_exec_time"]=="N")
		$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"] = $_GET["show_include_exec_time"];
}

//magic parameters: show include areas
if(isset($_GET["bitrix_include_areas"]) && $_GET["bitrix_include_areas"] <> "")
	$GLOBALS["APPLICATION"]->SetShowIncludeAreas($_GET["bitrix_include_areas"]=="Y");

//magic sound
if($GLOBALS["USER"]->IsAuthorized())
{
	$cookie_prefix = COption::GetOptionString('main', 'cookie_name', 'BITRIX_SM');
	if(!isset($_COOKIE[$cookie_prefix.'_SOUND_LOGIN_PLAYED']))
		$GLOBALS["APPLICATION"]->set_cookie('SOUND_LOGIN_PLAYED', 'Y', 0);
}

//magic cache
\Bitrix\Main\Page\Frame::shouldBeEnabled();

//magic short URI
if(defined("BX_CHECK_SHORT_URI") && BX_CHECK_SHORT_URI && CBXShortUri::CheckUri())
{
	//local redirect inside
	die();
}

foreach(GetModuleEvents("main", "OnBeforeProlog", true) as $arEvent)
	ExecuteModuleEventEx($arEvent);

if((!defined("NOT_CHECK_PERMISSIONS") || NOT_CHECK_PERMISSIONS!==true) && (!defined("NOT_CHECK_FILE_PERMISSIONS") || NOT_CHECK_FILE_PERMISSIONS!==true))
{
	$real_path = $request->getScriptFile();

	if(!$GLOBALS["USER"]->CanDoFileOperation('fm_view_file', array(SITE_ID, $real_path)) || (defined("NEED_AUTH") && NEED_AUTH && !$GLOBALS["USER"]->IsAuthorized()))
	{
		/** @noinspection PhpUndefinedVariableInspection */
		if($GLOBALS["USER"]->IsAuthorized() && $arAuthResult["MESSAGE"] == '')
			$arAuthResult = array("MESSAGE"=>GetMessage("ACCESS_DENIED").' '.GetMessage("ACCESS_DENIED_FILE", array("#FILE#"=>$real_path)), "TYPE"=>"ERROR");

		if(defined("ADMIN_SECTION") && ADMIN_SECTION==true)
		{
			if ($_REQUEST["mode"]=="list" || $_REQUEST["mode"]=="settings")
			{
				echo "<script>top.location='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';</script>";
				die();
			}
			elseif ($_REQUEST["mode"]=="frame")
			{
				echo "<script type=\"text/javascript\">
					var w = (opener? opener.window:parent.window);
					w.location.href='".$GLOBALS["APPLICATION"]->GetCurPage()."?".DeleteParam(array("mode"))."';
				</script>";
				die();
			}
			elseif(defined("MOBILE_APP_ADMIN") && MOBILE_APP_ADMIN==true)
			{
				echo json_encode(Array("status"=>"failed"));
				die();
			}
		}

		/** @noinspection PhpUndefinedVariableInspection */
		$GLOBALS["APPLICATION"]->AuthForm($arAuthResult);
	}
}

       //Do not remove this

if(isset($REDIRECT_STATUS) && $REDIRECT_STATUS==404)
{
	if(COption::GetOptionString("main", "header_200", "N")=="Y")
		CHTTP::SetStatus("200 OK");
}