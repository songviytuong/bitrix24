<?php
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/user_vars/admin/user_vars_list.php");
IncludeModuleLangFile(__FILE__);
global $APPLICATION;
if($APPLICATION->GetGroupRight("user_vars")>"D")
{
$aMenu[] = array(
    "parent_menu" => "global_menu_settings",
    "section" => "user_vars",
    "sort" => 1,
    "text" => GetMessage("LIPIT_MODULE"),
    "title" => GetMessage("LIPIT_MODULE"),
    "icon" => "user_menu_icon",
    "page_icon" => "user_menu_icon",
    "module_id" => "user_vars",
    "items_id" => "menu_user_vars",
    "items" => array(
            array(
                "text" => GetMessage('UV_TITLE'),
                "url" => "user_vars.php?lang=" . LANGUAGE_ID,
                "more_url" => Array("user_vars.php"),
                "title" => GetMessage('UV_TITLE'),
            ),
            array(
                "text" => GetMessage("LIST_MENU"),
                "url" => "user_vars_list.php?lang=" . LANGUAGE_ID,
                "more_url" => Array("user_vars_list.php","user_vars_edit.php"),
                "title" => GetMessage("LIST_MENU"),
            ),
        )
    );
return $aMenu;
}
return false;