<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
define("HELP_FILE", "user_vars/user_vars_edit.php");

ClearVars();

function UserInfo($USER_ID)
{
	if(intval($USER_ID) <= 0)
		return "";
	$user = CUser::GetByID($USER_ID);
	if($user_arr = $user->Fetch())
		return '[<a title="'.GetMessage("MAIN_USER_PROFILE").'" href="user_edit.php?ID='.$user_arr["ID"].'&amp;lang='.LANG.'">'.$user_arr["ID"].'</a>] ('.htmlspecialcharsbx($user_arr["LOGIN"]).') '.htmlspecialcharsbx($user_arr["NAME"]).' '.htmlspecialcharsbx($user_arr["LAST_NAME"]);
}

CModule::IncludeModule('user_vars');

$isAdmin = $USER->CanDoOperation('edit_other_settings');

IncludeModuleLangFile(__FILE__);

$APPLICATION->SetTitle(GetMessage("EDIT_TITLE"));

$message = null;
$bVarsFromForm = false;

if($_REQUEST["encoded"] == "Y"){
    CUtil::decodeURIComponent($_REQUEST["params"]);
}
$par = htmlspecialcharsbx($_REQUEST["params"]);

if($_SERVER['REQUEST_METHOD']=="POST" && ($_POST['save']<>"" || $_POST['apply']<>"") && check_bitrix_sessid())
{
    $arFields = array(
        "NAME" => $_POST['NAME'],
        "VALUE" => $_POST['VALUE'],
        "DESCRIPTION" => $_POST['DESCRIPTION']
    );

    if ($par != "") {
        $res = UserVars::Update($par, $arFields);
    } else {
        $ID = UserVars::Add($arFields);
        $res = ($par == "");
    }
    if ($res) {
        if ($apply <> "") {
            $_SESSION["SESS_ADMIN"]["VARS_EDIT_MESSAGE"] = array("MESSAGE" => GetMessage("edit_success"), "TYPE" => "OK");
            LocalRedirect("user_vars_edit.php?params=" . $par . "&lang=" . LANG);
        } else
            LocalRedirect(($_REQUEST["addurl"] <> "" ? $_REQUEST["addurl"] : "user_vars_list.php?lang=" . LANG));
    }
    else {
        if ($e = $APPLICATION->GetException())
            $message = new CAdminMessage(GetMessage("edit_error"), $e);
        $bVarsFromForm = true;
    }
}

if($par)
{
    $user_vars = UserVars::GetByID($par);
    if(!($user_vars_arr = $user_vars->ExtractFields("str_")))
            $par="";
}

if($bVarsFromForm)
	$DB->InitTableVarsForEdit("b_option", "", "str_");


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = array(
	array(
		"TEXT"=>GetMessage("MAIN_RECORDS_LIST"),
		"TITLE"=>GetMessage("MAIN_RECORDS_LIST"),
		"LINK"=>"user_vars_list.php?lang=".LANG,
		"ICON"=>"btn_list",
	)
);
if($par)
{
	$aMenu[] = array("SEPARATOR"=>"Y");
	$aMenu[] = array(
		"TEXT"=>GetMessage("EDIT_ADD"),
		"TITLE"=>GetMessage("EDIT_ADD"),
		"LINK"=>"user_vars_edit.php?lang=".LANG,
		"ICON"=>"btn_new",
	);
	$aMenu[] = array(
		"TEXT"=>GetMessage("EDIT_DELETE"),
		"TITLE"=>GetMessage("EDIT_DELETE"),
		"LINK"=>"javascript:if(confirm('".GetMessage("delete_conf")."')) window.location='user_vars_list.php?params=".$par."&lang=".LANG."&action=delete&".bitrix_sessid_get()."';",
		"ICON"=>"btn_delete",
	);
}
$context = new CAdminContextMenu($aMenu);
$context->Show();

if(is_array($_SESSION["SESS_ADMIN"]["VARS_EDIT_MESSAGE"]))
{
	CAdminMessage::ShowMessage($_SESSION["SESS_ADMIN"]["VARS_EDIT_MESSAGE"]);
	$_SESSION["SESS_ADMIN"]["VARS_EDIT_MESSAGE"]=false;
}

if($message)
	echo $message->Show();

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("edit_tab"), "TITLE"=>GetMessage("edit_tab")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>
<form method="POST" action="<?echo $APPLICATION->GetCurPage()?>" name="favform">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="params" value="<?= $par; ?>">
    <?if($_REQUEST["addurl"]<>""):?>
    <input type="hidden" name="addurl" value="<?echo htmlspecialcharsbx($_REQUEST["addurl"])?>">
    <?endif;?>
    <?php
    $tabControl->Begin();
    $tabControl->BeginNextTab();
    ?>
    <tr class="adm-detail-required-field">
        <td width="40%"><?= GetMessage("COL_NAME") ?></td>
        <td width="60%"><input type="text" name="NAME" size="45" maxlength="255" value="<?= $str_NAME ?>"></td>
    </tr>
    <tr class="adm-detail-required-field">
        <td><?= GetMessage("COL_VALUE") ?></td>
        <td><input type="text" name="VALUE" maxlength="2000" value="<?= $str_VALUE ?>"></td>
    </tr>
    <tr>
        <td><?= GetMessage("COL_DESCRIPTION") ?></td>
        <td><input type="text" name="DESCRIPTION" maxlength="2000" value="<?= $str_DESCRIPTION ?>"></td>
    </tr>

    <?
    if(!$isAdmin):
    ?>
    <tr class="heading">
        <td colspan="2"><?echo GetMessage("fav_edit_admin")?></td>
    </tr>
    <tr>
        <td><?echo GetMessage("fav_edit_common")?></td>
        <td><input type="checkbox" name="COMMON" value="Y"<?if($str_COMMON == "Y") echo " checked"?> onClick="EnableControls(this.checked)"></td>
    </tr>
    <tr>
        <td><?echo GetMessage("fav_edit_user")?></td>
        <td><?
            $sUser = "";
            if($ID>0)
            $sUser = UserInfo($str_USER_ID);
            echo FindUserID("USER_ID", ($str_USER_ID>0? $str_USER_ID:""), $sUser, "favform", "10", "", " ... ", "", "");
            ?>
        </td>
    </tr>
    <tr>
        <td><?echo GetMessage("fav_edit_modules")?></td>
        <td>
            <select name="MODULE_ID">
                <option value=""><?echo GetMessage("fav_edit_modules_not")?></option>
                <?
                $a = CModule::GetDropDownList();
                while($ar = $a->Fetch()):
                ?>
                <option value="<?echo htmlspecialcharsbx($ar["REFERENCE_ID"])?>"<?if($ar["REFERENCE_ID"] == $str_MODULE_ID) echo " selected"?>><?echo htmlspecialcharsbx($ar["REFERENCE"])?></option>
                <?endwhile?>
            </select>
            <script type="text/javascript">
            function EnableControls(checked)
            {
            document.favform.USER_ID.disabled = document.favform.FindUser.disabled = checked;
            document.favform.MODULE_ID.disabled = !checked;
            }
            EnableControls(document.favform.COMMON.checked);
            </script>
        </td>
    </tr>
    <?
    endif; //$isAdmin
    ?>
    <?php
    $tabControl->Buttons(array(
    "disabled"=>false,
    "back_url"=>($_REQUEST["addurl"]<>""? $_REQUEST["addurl"]:"user_vars_list.php?lang=".LANG),
    ));
    $tabControl->End();
    ?>
</form>

<?php
$tabControl->ShowWarnings("favform", $message);
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>