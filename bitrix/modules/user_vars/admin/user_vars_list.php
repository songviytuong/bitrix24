<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

if (!$USER->CanDoOperation('edit_own_profile') && !$USER->CanDoOperation('edit_other_settings') && !$USER->CanDoOperation('view_other_settings'))
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$isAdmin = $USER->CanDoOperation('edit_other_settings');
CModule::IncludeModule('user_vars');
IncludeModuleLangFile(__FILE__);

$sTableID = "tbl_option";
if ($isAdmin)
    $oSort = new CAdminSorting($sTableID, "id", "desc");
else
    $oSort = new CAdminSorting($sTableID, "sort", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);

function CheckFilter() {
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $f)
        global $$f;
    $date_1_ok = false;
    $date1_stm = MkDateTime(FmtDate($find_date1, "D.M.Y"), "d.m.Y");
    $date2_stm = MkDateTime(FmtDate($find_date2, "D.M.Y") . " 23:59", "d.m.Y H:i");
    if (!$date1_stm && strlen(trim($find_date1)) > 0)
        $lAdmin->AddFilterError(GetMessage("MAIN_WRONG_DATE_FROM"));
    else
        $date_1_ok = true;
    if (!$date2_stm && strlen(trim($find_date2)) > 0)
        $lAdmin->AddFilterError(GetMessage("MAIN_WRONG_DATE_TILL"));
    elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
        $lAdmin->AddFilterError(GetMessage("MAIN_FROM_TILL_DATE"));
    return count($lAdmin->arFilterErrors) == 0;
}

$FilterArr = Array(
    "find_name",
    "find_value",
    "find_description",
);

$lAdmin->InitFilter($FilterArr);
$arFilter = Array();
if (CheckFilter()) {
    $arFilter = Array(
        "NAME" => ($find != "" && $find_type == "name" ? $find : $find_name),
        "VALUE" => ($find != "" && $find_type == "value" ? $find : $find_value),
        "DESCIPTION" => ($find != "" && $find_type == "description" ? $find : $find_description),
    );
}

if ($lAdmin->EditAction()) {
    foreach ($FIELDS as $ID => $arFields) {
//        if($ID <= 0)
//                continue;
//        if(!$lAdmin->IsUpdated($ID))
//                continue;
        if (!UserVars::Update($ID, $arFields)) {
            $e = $APPLICATION->GetException();
            $lAdmin->AddUpdateError(($e ? $e->GetString() : GetMessage("UPDATE_ERROR")), $ID);
        }
    }
}


if (($arID = $lAdmin->GroupAction())) {
    if ($_REQUEST['action_target'] == 'selected') {
        $rsData = UserVars::GetList();
        foreach($rsData as $arRes){
            $arID[] = $arRes['MODULE_ID']."|".$arRes['NAME'];
        }
    } else {
        //REQUEST FROM EDIT PAGE
        $arID[] = $params;
    }
    
    foreach ($arID as $ID) {
//		if($ID <= 0)
//			continue;
        switch ($_REQUEST['action']) {
            case "delete":
                if (!UserVars::Delete($ID))
                    $lAdmin->AddGroupError(GetMessage("REMOVE_ERROR"), $params);
                break;
        }
    }
}

$rsData = UserVars::GetList(); //array($by=>$order), $arFilter
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("vars_list_nav")));


if ($isAdmin) {
    $aHeaders[] = array("id" => "MODULE_ID", "content" => GetMessage("COL_MODULE_ID"), "sort" => "MODULE_ID", "default" => true);
}

$aHeaders = array(
    array("id" => "MODULE_ID", "content" => GetMessage("COL_MODULE_ID"), "sort" => "MODULE_ID", "default" => true),
    array("id" => "NAME", "content" => GetMessage("COL_NAME"), "sort" => "NAME", "default" => true),
    array("id" => "VALUE", "content" => GetMessage("COL_VALUE"), "sort" => "VALUE", "default" => true),
    array("id" => "DESCRIPTION", "content" => GetMessage("COL_DESCRIPTION"), "sort" => "DESCRIPTION", "default" => true),
);

$lAdmin->AddHeaders($aHeaders);

while ($arRes = $rsData->NavNext(true, "f_")) {
    $params = $arRes["MODULE_ID"] . "|" . $arRes["NAME"];

    $row = & $lAdmin->AddRow($params, $arRes);
    $row->AddViewField("MODULE_ID", $f_MODULE_ID);
    $row->AddInputField("NAME", array("size" => 20));
    $row->AddViewField("NAME", $f_NAME);
    $row->AddInputField("VALUE", array("size" => 20));
    $row->AddViewField("VALUE", $f_VALUE);
    $row->AddInputField("DESCRIPTION", array("size" => 20));
    $row->AddViewField("DESCRIPTION", $f_DESCRIPTION);

    $arActions = Array(
        array(
            "ICON" => "edit",
            "DEFAULT" => true,
            "TEXT" => GetMessage("CHANGE"),
            "ACTION" => $lAdmin->ActionRedirect("user_vars_edit.php?params=" . $params)
        ),
        array("SEPARATOR" => true),
        array(
            "ICON" => "delete",
            "TEXT" => GetMessage("DELETE"),
            "ACTION" => "if(confirm('" . GetMessage("delete_conf") . "')) " . $lAdmin->ActionDoGroup($params, "delete")
        ),
    );

    $row->AddActions($arActions);
}

$lAdmin->AddGroupActionTable(Array(
    "delete" => true,
    //"activate" => GetMessage("MAIN_ADMIN_LIST_ACTIVATE"),
    //"deactivate" => GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"),
));

$aContext = array(
    array(
        "TEXT" => GetMessage("ADD"),
        "LINK" => "user_vars_edit.php?lang=" . LANG,
        "TITLE" => GetMessage("ADD"),
        "ICON" => "btn_new",
    ),
);

$lAdmin->AddAdminContextMenu($aContext);
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("LIST_MENU"));
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
?>

<?php
$arFRows = array(
    "find_name" => GetMessage("find_name"),
    "find_value" => GetMessage("find_value"),
    "find_description" => GetMessage("find_description")
);
$oFilter = new CAdminFilter($sTableID . "_filter", $arFRows);
?>
<form name="form1" method="GET" action="<?= $APPLICATION->GetCurPage() ?>">
    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
<?php $oFilter->Begin(); ?>
    <tr>
        <td><b><?php echo GetMessage("find") ?></b></td>
        <td>
            <input type="text" size="25" name="find" value="<?echo htmlspecialcharsbx($find)?>" title="<?echo GetMessage("fav_list_flt_find_title")?>">
<?php
$arr = array(
    "reference" => array(
        GetMessage("find_name"),
        GetMessage("find_value"),
        GetMessage("find_description"),
    ),
    "reference_id" => array(
        "NAME",
        "VALUE",
        "DESCRIPTION",
    )
);
echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
?>
        </td>
    </tr>
    <tr>
        <td><?echo GetMessage("find_name")?></td>
        <td><input type="text" name="find_name" size="40" value="<?echo htmlspecialcharsbx($find_name)?>"><?= ShowFilterLogicHelp() ?></td>
    </tr>
    <tr>
        <td><?echo GetMessage("find_value")?></td>
        <td><input type="text" name="find_value" size="40" value="<?echo htmlspecialcharsbx($find_value)?>"><?= ShowFilterLogicHelp() ?></td>
    </tr>
    <tr>
        <td><?echo GetMessage("find_description")?></td>
        <td><input type="text" name="find_description" size="40" value="<?echo htmlspecialcharsbx($find_description)?>"><?= ShowFilterLogicHelp() ?></td>
    </tr>
<?php
$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "form1"));
$oFilter->End();
?>
</form>
<?php $lAdmin->DisplayList(); ?>
<?php require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>