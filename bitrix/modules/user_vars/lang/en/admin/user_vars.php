<?
$MESS["UV_TITLE"] = "Custom variables";
$MESS["UV_BUTTON_SAVE"] = "Update";
$MESS["UV_TABLE_HEAD_NAME"] = "Name";
$MESS["UV_TABLE_HEAD_VALUE"] = "Value";
$MESS["UV_TABLE_HEAD_DESCRIPTION"] = "Description";
$MESS["UV_TABLE_HEAD_DEL"] = "Delete";
$MESS["UV_TABLE_HEAD_NAME_NOTICE"] = "May contain only letters, numbers and underscores.";
?>