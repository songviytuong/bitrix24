<?
$MESS["LIST_MENU"] = "List";
$MESS["COL_MODULE_ID"] = "MODULE_ID";
$MESS["COL_NAME"] = "Name";
$MESS["COL_VALUE"] = "Value";
$MESS["COL_DESCRIPTION"] = "Description";
$MESS["CHANGE"] = "Edit";
$MESS["COPY"] = "Copy";
$MESS["DELETE"] = "Delete";
$MESS["ADD"] = "Add";
$MESS["REMOVE_ERROR"] = "Error";
$MESS["UPDATE_ERROR"] = "Error";
$MESS["vars_list_nav"] = "Variables";
$MESS["find_name"] = "Name";
$MESS["find_value"] = "Value";
$MESS["find_description"] = "Description";
$MESS["find_module_id"] = "Module ID";
$MESS["find_filter"] = "Filter";
$MESS["find"] = "Find";
$MESS["delete_conf"] = "Are you sure you want to delete the selected recoreds?";
?>