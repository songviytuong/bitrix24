<?php
/*  Import IncludeModule form
 *  Call: CForm:err_mess()
 */
CModule::IncludeModule('form');
//CDBResult
class UserVars extends CAllOption {
    
    const TABLE_NAME = 'b_option';
    const MODULE_NAME = 'user_vars';
    
    public static function GetList() {
        global $DB;
        $optionList = array();
        $err_mess = (CForm::err_mess())."<br>Function: GetList()<br>Line: ";
        $sql = "SELECT MODULE_ID, NAME, VALUE, DESCRIPTION, SITE_ID FROM ".self::TABLE_NAME." WHERE MODULE_ID='" . $DB->ForSql(self::MODULE_NAME) . "'";
        $res = $DB->Query($sql, false, $err_mess.__LINE__);
        while ($ar = $res->Fetch()) {
            $optionList[] = $ar;
        }
        return $optionList;
    }
    
    public static function GetListByParams($params){
        global $DB;
        $params = explode("|", $params);
        $err_mess = (CForm::err_mess())."<br>Function: GetListByParams()<br>Line: ";
        $sql = "SELECT * FROM `b_option` WHERE MODULE_ID = '$params[0]' and NAME= '$params[1]'";
        $res = $DB->Query($sql, false, $err_mess.__LINE__);
        if ($arResult = $res->Fetch())
        {
            return $arResult;
        }
        return false;
    }
    
    public static function GetByID($params)
    {
        global $DB;
        $params = explode("|", $params);
        $err_mess = (CForm::err_mess())."<br>Function: GetByID()<br>Line: ";
        $sql = "SELECT MODULE_ID,NAME,VALUE,DESCRIPTION FROM `b_option` WHERE MODULE_ID = '$params[0]' and NAME= '$params[1]'";
        return ($DB->Query($sql, false, $err_mess.__LINE__));
    }
    public static function Add($arFields, $checkDuplicate = false)
    {
        
        global $DB;
        $err_mess = (CForm::err_mess())."<br>Function: Add()<br>Line: ";
        $codes = new CHotKeysCode;
        $codeID=$codes->Add(array(
            "MODULE_ID"=> "user_vars",
            "NAME"=>$arFields["NAME"],
            "VALUE"=>$arFields["VALUE"],
            "DESCRIPTION"=>$arFields["DESCRIPTION"]
        ));
        
        $codes->Update($codeID,array(
            "MODULE_ID"=>"user_vars",
            "NAME"=>$arFields["NAME"],
        ));

        $arFields["MODULE_ID"]= "user_vars";
                
        $ID = $DB->Add("b_option", $arFields, $err_mess.__LINE__);
        return $ID;
    }
    
    public static function Delete($params)
    {
        global $DB;
        $params = explode("|", $params);
        return ($DB->Query("DELETE FROM `b_option` WHERE `MODULE_ID` = '".$params[0]."' and `NAME`= '".$params[1]."'", false, "File: ".__FILE__."<br>Function: Delete()<br>Line: ".__LINE__));
    }
    
    public static function Update($params, $arFields)
    {
        global $DB;
        $params = explode("|", $params);
        $strUpdate = $DB->PrepareUpdate("b_option", $arFields);
        if($strUpdate!="")
        {
            $strSql = "UPDATE `b_option` SET ".$strUpdate." WHERE `MODULE_ID`= '".$params[0]."' and `NAME`= '".$params[1]."'";
            if(!$DB->Query($strSql))
                return false;
        }
        return true;
    }
    
    public static function ClearOptions() {
        parent::RemoveOption(self::MODULE_NAME);
    }

    public static function SetVar($name, $value = "", $desc = "") {
        return parent::SetOptionString(self::MODULE_NAME, $name, $value, $desc);
    }

    public static function GetVar($name, $def = "", $site = false, $bExactSite = false) {
        return parent::GetOptionString(self::MODULE_NAME, $name, $def, $site, $bExactSite);
    }

}
