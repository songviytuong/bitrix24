<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="intask-main data-table">
    <thead>
    <tr class="intask-row">
            <th class="intask-cell" width="0%">&nbsp;</th>
            <th class="intask-cell" width="30%"><?= $arResult["ALLOWED_FIELDS"]["MODULE_ID"]["NAME"] ?></th>
            <th class="intask-cell" width="30%"><?= $arResult["ALLOWED_FIELDS"]["NAME"]["NAME"] ?></th>
            <th class="intask-cell" width="60%"><?= $arResult["ALLOWED_FIELDS"]["VALUE"]["NAME"] ?></th>
            <th class="intask-cell" width="60%"><?= $arResult["ALLOWED_FIELDS"]["DESCRIPTION"]["NAME"] ?></th>
            <th></th>
    </tr>
    </thead>
    <?php
        foreach($arResult["LIST"] as $row):
    ?>
    <tr id="vars_row_<?=$row["MODULE_ID"]."|".$row["NAME"];?>">
            <td>
                <table cellpadding="0" cellspacing="0" border="0" class="intask-dropdown-pointer" onmouseover="this.className+=' intask-dropdown-pointer-over';" onmouseout="this.className=this.className.replace(' intask-dropdown-pointer-over', '');" onclick="if(window.ITSDropdownMenu != null){window.ITSDropdownMenu.ShowMenu(this, oObjectITS['intask_s<?= $arMeeting["ID"] ?>'], document.getElementById('intask_s<?= $arMeeting["ID"] ?>'))}" title="<?= GetMessage("INTDT_ACTIONS") ?>" id="intask_table_s<?= $arMeeting["ID"] ?>"><tr>
							<td>
								<div class="controls controls-view show-action">
									<a href="javascript:void(0);" class="action">
										<div id="intask_s<?= $arMeeting["ID"] ?>" class="empty"></div>
									</a>
								</div></td>
						</tr></table>
            </td>
            <td class="bizproc-field-id">
                    <?=$row["MODULE_ID"];?>
            </td>
            <td class="bizproc-field-name">
                    <?=$row["NAME"];?>
            </td>
            <td class="bizproc-field-value">
                    <?=$row["VALUE"];?>
            </td>
            <td class="bizproc-field-description">
                    <?=$row["DESCRIPTION"];?>
            </td>
            <td class="bizproc-field-description">
                <a href="" onclick="BXDeleteVars('<?=$row["MODULE_ID"]."|".$row["NAME"];?>')">Xóa</a>
            </td>
        </tr>
        <?php  endforeach; ?>
</table>
<script type="text/javascript">
function BXDeleteVars(params)
{
	if (confirm('<?=CUtil::JSEScape(GetMessage('delete_conf'))?>'))
	{
		var row = BX('vars_row_' + params);
                var url = '<?=CUtil::JSEscape($arParams['VARS_URL'])?>&DELETE=Y&<?=bitrix_sessid_get()?>';
                url = url.replace('#PARAMS#', params);
		BX.showWait(row);
		BX.ajax.get(url, function()
		{
			BX.closeWait(row);
			row.parentNode.removeChild(row);
		});
	}
}
</script> 